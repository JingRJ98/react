import {
  enableIsInputPending,
  enableIsInputPendingContinuous,
  frameYieldMs,
  continuousYieldMs,
  maxYieldMs,
} from '../SchedulerFeatureFlags';
import { push, pop, peek } from '../SchedulerMinHeap';
import {
  ImmediatePriority,
  UserBlockingPriority,
  NormalPriority,
  LowPriority,
  IdlePriority,
  PriorityLevel,
} from '../SchedulerPriorities';

/**
 * 返回一个时间戳 比Date.now()更加精确
 * @returns performance.now()
 */
const getCurrentTime = () => performance.now();

// Max 31 bit integer. The max integer size in V8 for 32-bit systems.
// 2^30 - 1 = 1073741823
// 0b 1111 1111 1111 1111 1111 1111 1111 11
const maxSigned31BitInt = 1073741823;

/** 最高优先级的 立即过期 */
const IMMEDIATE_PRIORITY_TIMEOUT = -1;
// Eventually times out
const USER_BLOCKING_PRIORITY_TIMEOUT = 250; // 250ms过期的
const NORMAL_PRIORITY_TIMEOUT = 5000; // 5000ms过期的
const LOW_PRIORITY_TIMEOUT = 10000; // 10000ms过期的
/** 永远不会过期的 */
const IDLE_PRIORITY_TIMEOUT = maxSigned31BitInt;

// 任务都会按照最小堆排序
/** 最小堆 */
const taskQueue = [];
/** 最小堆 */
const timerQueue = [];

// Incrementing id counter. Used to maintain insertion order.
let taskIdCounter = 1;

let currentTask = null;
let currentPriorityLevel = NormalPriority;

// This is set while performing work, to prevent re-entrance.
let isPerformingWork = false;
let isHostCallbackScheduled = false;
let isHostTimeoutScheduled = false;

// navigator.scheduling.isInputPending()
// 是这 facebook 官方贡献给 Chromium 的 api, 现在已经列入 W3C 标准
// 用于判断是否有输入事件(包括: input 框输入事件, 点击事件等)
const isInputPending = navigator.scheduling.isInputPending
const continuousOptions = { includeContinuous: enableIsInputPendingContinuous };

/**
 * 找一找timerQueue有没有能够转移进来taskQueue的， 通过判断Queue中的startTime和currentTime进行对比
 * 检查不再延迟的任务并将其添加到队列中。
 * @param {number} currentTime
 */
function advanceTimers(currentTime) {
  // 寻找timerQueue堆中sortIndex最小的节点
  let timer = peek(timerQueue);
  while (timer !== null) {
    if (timer.callback === null) {
      // Timer 被取消了, 直接扔掉
      pop(timerQueue);
    } else if (timer.startTime <= currentTime) {
      // timer的死期到了,准备上路, 转移到taskQueue
      pop(timerQueue);
      // sortIndex设置为过期时间, 即过期时间越小的进入最小堆后越在上面
      timer.sortIndex = timer.expirationTime;
      push(taskQueue, timer);
    } else {
      // 如果当前小顶堆取出的过期时间最小的 timer的callback不是null 且死期也没到
      // 剩下的timer的死期就更靠后了, 不再处理, 直接return
      return;
    }
    // 遍历timerQueue, 前面的timer都从timerQueue中pop出去了
    timer = peek(timerQueue);
  }
}

function handleTimeout(currentTime) {
  isHostTimeoutScheduled = false;
  advanceTimers(currentTime);

  if (!isHostCallbackScheduled) {
    if (peek(taskQueue) !== null) {
      isHostCallbackScheduled = true;
      requestHostCallback(flushWork);
    } else {
      const firstTimer = peek(timerQueue);
      if (firstTimer !== null) {
        requestHostTimeout(handleTimeout, firstTimer.startTime - currentTime);
      }
    }
  }
}

/**
 *
 * @param {boolean} hasTimeRemaining
 * @param {number} initialTime
 */
function flushWork(hasTimeRemaining, initialTime) {
  // 1. 做好全局标记, 表示现在已经进入调度阶段
  isHostCallbackScheduled = false;
  if (isHostTimeoutScheduled) {
    // We scheduled a timeout but it's no longer needed. Cancel it.
    isHostTimeoutScheduled = false;
    cancelHostTimeout();
  }

  isPerformingWork = true;
  const previousPriorityLevel = currentPriorityLevel;
  try {
    // 2. 循环消费队列
    return workLoop(hasTimeRemaining, initialTime);
  } finally {
    // 3. 还原全局标记
    currentTask = null;
    currentPriorityLevel = previousPriorityLevel;
    isPerformingWork = false;
  }
}

/**
 * 任务调度循环 (React两大循环fiber构造循环, 任务调度循环)
 * @param {boolean} hasTimeRemaining
 * @param {number} initialTime
 * @returns {boolean} 返回是否还有剩余任务
 */
function workLoop(hasTimeRemaining, initialTime) {
  // 保存传入的时间
  let currentTime = initialTime;
  advanceTimers(currentTime);
  // 取出过期时间最近的任务(优先级最高的任务)
  currentTask = peek(taskQueue);
  while (currentTask !== null) {
    if (
      currentTask.expirationTime > currentTime &&
      (!hasTimeRemaining || shouldYieldToHost())
    ) {
      // 任务虽然没过期, 但是没有时间剩余 或者 应该让出主线程了(执行时间超过5ms), 都break
      break;
    }
    const callback = currentTask.callback;
    if (typeof callback === 'function') {
      currentTask.callback = null;
      currentPriorityLevel = currentTask.priorityLevel;
      const didUserCallbackTimeout = currentTask.expirationTime <= currentTime;
      // 执行回调
      const continuationCallback = callback(didUserCallbackTimeout);
      currentTime = getCurrentTime();
      // 回调完成, 判断是否还有连续(派生)回调
      if (typeof continuationCallback === 'function') {
        currentTask.callback = continuationCallback;
      } else {
        // 如果当前的任务还是优先级最高的 移除任务
        if (currentTask === peek(taskQueue)) {
          pop(taskQueue);
        }
      }
      advanceTimers(currentTime);
    } else {
      pop(taskQueue);
    }
    currentTask = peek(taskQueue);
  }
  // 返回是否还有剩余任务
  if (currentTask !== null) {
    // 如果task队列没有清空, 返回true. 等待调度中心下一次回调
    return true;
  } else {
    const firstTimer = peek(timerQueue);
    if (firstTimer !== null) {
      requestHostTimeout(handleTimeout, firstTimer.startTime - currentTime);
    }
    return false;
  }
}

/** 这个函数没有被用到 保留函数 */
function unstable_runWithPriority(priorityLevel, eventHandler) {
  switch (priorityLevel) {
    case ImmediatePriority:
    case UserBlockingPriority:
    case NormalPriority:
    case LowPriority:
    case IdlePriority:
      break;
    default:
      priorityLevel = NormalPriority;
  }

  const previousPriorityLevel = currentPriorityLevel;
  currentPriorityLevel = priorityLevel;

  try {
    return eventHandler();
  } finally {
    currentPriorityLevel = previousPriorityLevel;
  }
}

/** 这个函数没有被用到 保留函数 */
function unstable_next(eventHandler) {
  let priorityLevel;
  switch (currentPriorityLevel) {
    case ImmediatePriority:
    case UserBlockingPriority:
    case NormalPriority:
      // Shift down to normal priority
      priorityLevel = NormalPriority;
      break;
    default:
      // Anything lower than normal priority should remain at the current level.
      priorityLevel = currentPriorityLevel;
      break;
  }

  const previousPriorityLevel = currentPriorityLevel;
  currentPriorityLevel = priorityLevel;

  try {
    return eventHandler();
  } finally {
    currentPriorityLevel = previousPriorityLevel;
  }
}

/** 这个函数没有被用到 保留函数 */
function unstable_wrapCallback(callback) {
  const parentPriorityLevel = currentPriorityLevel;
  return function () {
    // This is a fork of runWithPriority, inlined for performance.
    const previousPriorityLevel = currentPriorityLevel;
    currentPriorityLevel = parentPriorityLevel;

    try {
      return callback.apply(this, arguments);
    } finally {
      currentPriorityLevel = previousPriorityLevel;
    }
  };
}

/**
 * 创建任务, 并将任务放入队列, 并执行
 * @param {PriorityLevel} priorityLevel 优先级的级别
 * @param {function} callback task.callback属性值
 * @param {object?} options v18.2.0所有调用本函数的地方都没有传入option
 * @returns {object} Task, 一个带有特定字段的对象
 */
function unstable_scheduleCallback(priorityLevel: PriorityLevel, callback, options) {
  const currentTime = getCurrentTime();

  // 任务开始调度的时间
  let startTime = currentTime;
  if (typeof options === 'object' && options !== null) {
    // 如果传入了配置延时时间, 就将startTime + delay
    const delay = options.delay;
    if (typeof delay === 'number' && delay > 0) {
      startTime += delay;
    }
  }

  // 根据传入的优先级级别, 设置任务的过期时间 越低的优先级, 过期时间越靠后
  let timeout;
  switch (priorityLevel) {
    case ImmediatePriority:
      // 最高优先级
      timeout = IMMEDIATE_PRIORITY_TIMEOUT; // 立即
      break;
    case UserBlockingPriority:
      // 次优先级
      timeout = USER_BLOCKING_PRIORITY_TIMEOUT; // 250ms后过期
      break;
    case IdlePriority:
      // 空闲
      timeout = IDLE_PRIORITY_TIMEOUT; // 2**30 - 1ms后过期,几乎无限
      break;
    case LowPriority:
      // 低优先级
      timeout = LOW_PRIORITY_TIMEOUT; // 10000ms后过期
      break;
    case NormalPriority:
    default:
      // 普通优先级和默认
      timeout = NORMAL_PRIORITY_TIMEOUT; // 5000ms后过期
      break;
  }

  // 过期时间 越低的优先级, ddl越靠后
  const expirationTime = startTime + timeout;

  /**
   * 注意task中没有next属性, 它不是一个链表
   * 其顺序是通过堆排序来实现的(小顶堆数组, 始终保证数组中的第一个task对象优先级最高).
   */
  const newTask = {
    /** 唯一标识 */
    id: taskIdCounter++,
    /** task 最核心的字段, 指向react-reconciler包所提供的回调函数. */
    callback,
    /** 优先级 */
    priorityLevel,
    /** 一个时间戳,代表 task 的开始时间 */
    startTime,
    /** 过期时间 */
    expirationTime,
    /**
     * 控制 task 在队列中的次序, 值越小的越靠前
     * 这里的-1其实没有意义, 下面都会改掉
     */
    sortIndex: -1,
  };

  if (startTime > currentTime) {
    // 如果传入的option里面的delay是负数才会走进来
    // 但是v18.2.0所有调用本函数的地方都没有传入option
    newTask.sortIndex = startTime;
    push(timerQueue, newTask);
    if (peek(taskQueue) === null && newTask === peek(timerQueue)) {
      // All tasks are delayed, and this is the task with the earliest delay.
      if (isHostTimeoutScheduled) {
        // Cancel an existing timeout.
        cancelHostTimeout();
      } else {
        isHostTimeoutScheduled = true;
      }
      // Schedule a timeout.
      requestHostTimeout(handleTimeout, startTime - currentTime);
    }
  } else {
    newTask.sortIndex = expirationTime;
    // 任务会进入taskQueue小顶堆 按照过期时间排列
    push(taskQueue, newTask);
    // Schedule a host callback, if needed. If we're already performing work,
    // wait until the next time we yield.
    if (!isHostCallbackScheduled && !isPerformingWork) {
      isHostCallbackScheduled = true;
      requestHostCallback(flushWork);
    }
  }

  return newTask;
}

/** 这个函数没有被用到 保留函数 */
function unstable_continueExecution() {
  if (!isHostCallbackScheduled && !isPerformingWork) {
    isHostCallbackScheduled = true;
    requestHostCallback(flushWork);
  }
}

/** 这个函数没有被用到 保留函数 */
function unstable_getFirstCallbackNode() {
  return peek(taskQueue);
}

function unstable_cancelCallback(task) {

  // Null out the callback to indicate the task has been canceled. (Can't
  // remove from the queue because you can't remove arbitrary nodes from an
  // array based heap, only the first one.)
  task.callback = null;
}

function unstable_getCurrentPriorityLevel() {
  return currentPriorityLevel;
}

let isMessageLoopRunning = false;
let scheduledHostCallback = null;
let taskTimeoutID = -1;

/** 时间切片周期, 默认是5ms(如果一个task运行超过该周期, 下一个task执行之前, 会把控制权归还浏览器) */
let frameInterval = frameYieldMs;
/** 50ms */
const continuousInputInterval = continuousYieldMs;
/** 300ms */
const maxInterval = maxYieldMs;
let startTime = -1;

let needsPaint = false;

/**
 * 是否让出主线程 (是否可中断)
 * 通过判断现在和任务起始时间之间的时间差有无超过一个时间切片周期(5ms)
 * @returns {boolean}
 */
function shouldYieldToHost() {
  const timeElapsed = getCurrentTime() - startTime;
  if (timeElapsed < frameInterval) {
    // 主线程之被阻塞了很短的时间, 小于一个时间帧 5ms, 不用让出主线程
    return false;
  }

  // 主线程已被阻塞了不可忽略的时间, 放弃对主线程的控制，以便浏览器可以执行高优先级任务, 主要是绘制和用户输入。
  // 如果有待处理的paint或待处理的输入，那么应该让出主线程
  // 但如果两者都没有，那么我们就可以在保持响应性的同时减少产量。 无论如何，我们最终都会屈服，因为可能存在未调用“requestPaint”或其他主线程任务（例如网络事件）的待处理绘制。
  if (enableIsInputPending) {
    if (needsPaint) {
      // 存在一个等待的 paint, 由requestPaint改动的, 需要让出主线程
      return true;
    }
    if (timeElapsed < continuousInputInterval) {
      // 超过5ms 但是小于50ms
      // 此时模棱两可 需要看具体的情况, 比如鼠标点击就需要让出, 一些无关紧要的不需要让出
      // 用原生提供的api来判断  navigator.scheduling.isInputPending
      return isInputPending();
    } else if (timeElapsed < maxInterval) {
      // 超过50ms 但是小于300ms
      // 仍然是模棱两可 如果存在待处理的离散或连续输入就让出
      // 用原生提供的api来判断  navigator.scheduling.isInputPending
      // 注意和上面小于50ms的区别在于这里传入了 continuousOptions 参数
      return isInputPending(continuousOptions);
    } else {
      // 已经占据主线程300ms及以上了, 无论如何也得让出主线程
      return true;
    }
  }

  // 注意上面一大堆的判断可能是不需要的(?)
  // 只要超过5ms就让出主线程
  return true;
}

function requestPaint() {
  if (enableIsInputPending) {
    needsPaint = true;
  }
}

/** 这个函数没有被用到 保留函数 */
function forceFrameRate(fps) {
  if (fps < 0 || fps > 125) {
    // Using console['error'] to evade Babel and ESLint
    console['error'](
      'forceFrameRate takes a positive int between 0 and 125, ' +
      'forcing frame rates higher than 125 fps is not supported',
    );
    return;
  }
  if (fps > 0) {
    frameInterval = Math.floor(1000 / fps);
  } else {
    // reset the framerate
    frameInterval = frameYieldMs;
  }
}

/**
 * 接收 MessageChannel 消息
 */
const performWorkUntilDeadline = () => {
  // scheduledHostCallback 就是 requestHostCallback 中传入的参数callback flushWor
  if (scheduledHostCallback !== null) {
    const currentTime = getCurrentTime();
    // 跟踪开始时间，以便我们可以测量主线程被阻塞的时间
    startTime = currentTime;
    const hasTimeRemaining = true;

    // 如果调度程序任务抛出异常，退出当前浏览器任务，以便可以观察到错误。
    // 故意不使用 try-catch，因为这会使一些调试技术变得更加困难。
    // 这里使用的手段是 如果“scheduledHostCallback”错误，则“hasMoreWork”将继续保持为真，将继续工作循环。
    let hasMoreWork = true;
    try {
      // scheduledHostCallback 是 flushWork 函数
      // flushWork执行完 return workLoop的结果, 即是否还有剩余任务
      hasMoreWork = scheduledHostCallback(hasTimeRemaining, currentTime);
    } finally {
      if (hasMoreWork) {
        // 走到这里说明 try里面的函数报错了hasMoreWork保持为真, 或者workLoop函数返回的还有剩余任务 将继续工作循环。
        // 通过 MessageChannel 发送消息, 继续performWorkUntilDeadline
        schedulePerformWorkUntilDeadline();
      } else {
        // scheduledHostCallback作为函数没有报错, 且这个函数的返回值为false
        // scheduledHostCallback就是flushWork函数, flushWork函数里面返回的又是workLoop函数的返回值, 即是否有多余任务

        // 循环 关闭!
        isMessageLoopRunning = false;
        scheduledHostCallback = null;
      }
    }
  } else {
    // scheduledHostCallback是null直接重置状态, 循环 关闭!
    isMessageLoopRunning = false;
  }

  needsPaint = false;
};

// 我们更喜欢 MessageChannel 因为 4ms setTimeout 限制。(?)
const channel = new MessageChannel();
channel.port1.onmessage = performWorkUntilDeadline;
// 在支持setImmediate的环境中, 比如node, scheduler适用setImmediate来调度宏任务
// 浏览器环境使用MessageChannel来调度宏任务
const schedulePerformWorkUntilDeadline = () => {
  channel.port2.postMessage(null);
};

/**
 * 请求及时回调, 对应下面的异步回调`requestHostTimeout`
 * 请注意! MessageChannel在浏览器事件循环中属于宏任务, 所以调度中心永远是 异步 执行回调函数.
 * 所以这里我没有叫请求同步回调, 而是请求及时回调
 * @param {function} callback
 */
function requestHostCallback(callback) {
  // 1. 保存callback
  // performWorkUntilDeadline里面要用 scheduledHostCallback
  scheduledHostCallback = callback;
  if (!isMessageLoopRunning) {
    // 循环 启动!
    isMessageLoopRunning = true;
    // 2. 通过 MessageChannel 发送消息
    schedulePerformWorkUntilDeadline();
  }
}

/**
 * 请求异步回调
 * ms毫秒后执行回调, 并且回调的参数是那时的currentTime
 * @param {function} callback
 * @param {number} ms
 */
function requestHostTimeout(callback, ms) {
  taskTimeoutID = setTimeout(() => {
    callback(getCurrentTime());
  }, ms);
}
/** 取消异步回调 */
function cancelHostTimeout() {
  clearTimeout(taskTimeoutID);
  taskTimeoutID = -1;
}

const unstable_requestPaint = requestPaint;

export {
  ImmediatePriority as unstable_ImmediatePriority,
  UserBlockingPriority as unstable_UserBlockingPriority,
  NormalPriority as unstable_NormalPriority,
  IdlePriority as unstable_IdlePriority,
  LowPriority as unstable_LowPriority,
  unstable_scheduleCallback,
  unstable_cancelCallback,
  unstable_getCurrentPriorityLevel,
  shouldYieldToHost as unstable_shouldYield,
  unstable_requestPaint,
  /********************************************************************************/
  unstable_wrapCallback, /********************************/// 这个函数没有被用到 保留函数
  unstable_runWithPriority, /*****************************/// 这个函数没有被用到 保留函数
  unstable_next, /****************************************/// 这个函数没有被用到 保留函数
  unstable_continueExecution, /***************************/// 这个函数没有被用到 保留函数
  unstable_getFirstCallbackNode, /************************/// 这个函数没有被用到 保留函数
  forceFrameRate as unstable_forceFrameRate, /************/// 这个函数没有被用到 保留函数
  /********************************************************************************/
};
