export const enableIsInputPending = false;
export const enableProfiling = false;
export const enableIsInputPendingContinuous = false;
/** 时间切片周期, 默认是5ms(如果一个task运行超过该周期, 下一个task执行之前, 会把控制权归还浏览器) */
export const frameYieldMs = 5;
export const continuousYieldMs = 50;
export const maxYieldMs = 300;
