export type PriorityLevel = 0 | 1 | 2 | 3 | 4 | 5; // 优先级级别

export const NoPriority = 0; // 没有被用到
export const ImmediatePriority = 1; // 最高优先级
export const UserBlockingPriority = 2; // 次优先级(用户阻塞优先级)
export const NormalPriority = 3; // 普通优先级
export const LowPriority = 4; // 低优先级
export const IdlePriority = 5; // 无限低的优先级(空闲优先级)
