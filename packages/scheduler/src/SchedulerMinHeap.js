type Node = {
  id: number,
  sortIndex: number,
};
type Heap = Array<Node>;

////////////////////////////////////////////////////////////////////////////////
// 最小堆(O(1)时间复杂度取到堆顶元素), 注意只是小顶, 而非排序
// 表面上是个数组, 其实可以当成树看待
// 或者说一个树结构, 可以用数组表示, 小顶堆为父节点的值一定小于等于左右子节点的值, 根节点的值最小
//           0
//         /   \
//        1     2
//       / \   / \
//      3   4 5   6
//     / \
//    7   8
////////////////////////////////////////////////////////////////////////////////


/**
 * 添加新节点, 添加之后, 需要调用`siftUp`函数向上调整堆.
 * @param {Heap} heap 小顶堆
 * @param {*} node
 */
export const push = (heap: Heap, node: Node): void => {
  heap.push(node);
  // 要把添加的叶子节点往上找到它该在的位置
  siftUp(heap, node, heap.length - 1);
}

/**
 * 查看堆的顶点, 也就是优先级最高的`task`或`timer`
 * @param {Heap} heap 小顶堆
 */
export const peek = (heap: Heap): Node | null => heap[0] ?? null

/**
 * 将堆的顶点提取出来, 并删除顶点之后, 需要调用`siftDown`函数向下调整堆.
 * @param {Heap} heap
 * @returns {Node}
 */
export const pop = (heap: Heap): Node | null => {
  if (heap.length === 0) return null;

  // 其实相当于heap.shift(), 然后调整堆的顺序, 只不过数组的头操作比较消耗性能
  const first = heap[0];
  const last = heap.pop();
  if (last !== first) {
    // 如果堆中不止一个元素, 把尾巴的元素放在第一个 然后排序
    heap[0] = last;
    // 要把添加的根节点往下找到它该在的位置
    siftDown(heap, last, 0);
  }
  // 返回原来索引为0的元素(堆顶元素)
  return first;
}

/**
 * 向上调整小顶堆
 * 如果对heap数组执行的是push操作, 从后面加 需要调用`siftup` 三个参数分别是数组, 插入的对象, 最后一个元素的索引(即heap.length - 1)
 * 要把添加的叶子节点往上找到它该在的位置
 * @param {Heap} heap
 * @param {Node} node
 * @param {Number} i
 */
const siftUp = (heap, node, i) => {
  let index = i;
  // 陌生节点的尽头就是成为整个树的根, 即数组中索引为0的节点
  while (index > 0) {
    // 数组push, 相当于插入了一个最底层的叶子节点, 寻找父节点索引, 索引从0开始
    // 例如索引7,8的父节点索引为3, 减一再除2,然后取整
    //           0
    //         /   \
    //        1     2
    //       / \   / \
    //      3   4 5   6
    //     / \
    //    7   8
    const parentIndex = (index - 1) >>> 1;
    const parent = heap[parentIndex];
    if (compare(parent, node) > 0) {
      // 父节点更大就把当前节点和父节节点交换
      // 注意这里没有比较一层元素之间的大小, 只要能大过上级, 当前节点就和上级交换
      // 父节点被交换下来之后,虽然一定大于原来父的子节点, 但是不一定大得过同一层的其他节点
      heap[parentIndex] = node;
      heap[index] = parent;
      index = parentIndex;
    } else {
      // node已经找到该待的位置 直接return
      return;
    }
  }
}

/**
 * 向下调整小顶堆
 * 如果对heap数组执行的是类似unshift操作, 从前面加, 要把添加的节点往下找到它该在的位置
 * 需要调用`siftdown` 三个参数分别是数组, 插入的对象, 第一个元素的索引(即0)
 * @param {*} heap
 * @param {*} node
 * @param {*} i
 * @returns
 */
function siftDown(heap, node, i) {
  let index = i;
  const length = heap.length;
  const halfLength = length >>> 1;
  while (index < halfLength) {
    //           0
    //         /   \
    //        1     2
    //       / \   / \
    //      3   4 5   6
    //     / \
    //    7   8
    // 左子节点的索引为(当前节点索引 + 1) * 2 - 1, 如3的左子节点索引是7
    const leftIndex = (index + 1) * 2 - 1;
    const left = heap[leftIndex];
    // 右子节点的索引为左子节点的索引 + 1, 如3的子节点索引是7, 8
    const rightIndex = leftIndex + 1;
    const right = heap[rightIndex];

    if (compare(left, node) < 0) {
      if (rightIndex < length && compare(right, left) < 0) {
        // 如果右子节点 < 左子节点 < 当前节点
        // 将当前节点和右子节点交换
        heap[index] = right;
        heap[rightIndex] = node;
        index = rightIndex;
      } else {
        // 如果左子节点 < 当前节点, 且左子节点 < 右子节点
        // 将当前节点和左子节点交换
        heap[index] = left;
        heap[leftIndex] = node;
        index = leftIndex;
      }
    } else if (rightIndex < length && compare(right, node) < 0) {
      // 如果右子节点 < 当前节点 < 左子节点
      // 将当前节点和右子节点交换
      heap[index] = right;
      heap[rightIndex] = node;
      index = rightIndex;
    } else {
      // 如果当前节点既不小于左子节点又不小于右子节点, 当前节点已经找到了该在的位置
      return;
    }
  }
}

/**
 * 优先按照sortIndex从小到大排序, sortIndex一样按照id排序
 * @param {Node} a
 * @param {Node} b
 * @returns {Number} 返回一个正数/负数 负数代表a小,否则b小
 */
const compare = (a, b) => {
  const diff = a.sortIndex - b.sortIndex;
  return diff !== 0 ? diff : a.id - b.id;
}
