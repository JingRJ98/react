# scheduler


## 简单的调度模型
```js
const arr = []

const scheduler = () => {
  const task = arr.pop()
  if(task){
    run(task)
  }
}
const run = (task) => {
  exec(task)
  scheduler()
}
```

## 调度相关: 请求或取消调度
- requestHostCallback 请求及时回调 (这里叫及时而不是同步的原因请看函数详细注释)
- requestHostTimeout 请求异步回调
- cancelHostTimeout  取消异步回调

## 时间切片(time slicing)相关: 执行时间分割, 让出主线程(把控制权归还浏览器, 浏览器可以处理用户输入, UI 绘制等紧急任务)
- getCurrentTime: 获取当前时间
- shouldYieldToHost: 是否让出主线程
- requestPaint: 请求绘制
- forceFrameRate: 强制设置 yieldInterval(从源码中的引用来看, 算一个保留函数, 其他地方没有用到)


## 循环

unstable_scheduleCallback 创建任务 存入任务队列
=> requestHostCallback(callback)
=> schedulePerformWorkUntilDeadline 发消息
=> performWorkUntilDeadline onMessage的回调
=> flushWork 是requestHostCallback的参数 
=> workLoop
=> 消费任务队列
=> workLoop返回是否还有剩余任务
=> performWorkUntilDeadline拿到workLoop的结果
=> 如果是true schedulePerformWorkUntilDeadline 发消息
=> performWorkUntilDeadline onMessage的回调

这个循环里面还涉及到时间切片让出主线程等细节

## 优先级转换
从React事件优先级到schedule优先级需要进行两步
1. lane转换成eventPriority
使用lanesToEventPriority()函数, 基本上是一个阶梯函数

2. EventPriority转换成schedule优先级
在ensureRootIsScheduled中 非同步lane会进行优先级转换

从schedule优先级到事件优先级转换
在getEventPriority()函数中