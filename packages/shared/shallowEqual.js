import is from './objectIs';
/**
 * Performs equality by iterating through keys on an object and returning false
 * when any key has values which are not strictly equal between the arguments.
 * Returns true when the values of all keys are strictly equal.
 */
function shallowEqual(objA: mixed, objB: mixed): boolean {
  if (is(objA, objB)) return true;

  // 比较两个对象结构一样, 所以排除null和非对象
  if (
    typeof objA !== 'object' || objA === null ||
    typeof objB !== 'object' || objB === null
  ) return false;

  const keysA = Object.keys(objA);
  const keysB = Object.keys(objB);

  if (keysA.length !== keysB.length) return false;

  for (let i = 0; i < keysA.length; i++) {
    const currentKey = keysA[i];
    // 原代码这里使用的是Object.prototype.hasOwnProperty.call(objB, currentKey)
    // 目的是防止objB的hasOwnProperty被改写
    // 这里为了简便直接使用实例去访问prototype上的hasOwnProperty
    if (!objB.hasOwnProperty(currentKey) || !is(objA[currentKey], objB[currentKey])) {
      // 如果objB没有这个key, 或者Object.is(两个对象对应key的value)不一样
      return false;
    }
  }

  return true;
}

export default shallowEqual;
