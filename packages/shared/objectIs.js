
// NaN === NaN  false
// Object.is(NaN, NaN) true

// +0 === -0  true
// Object.is(+0, -0)  false
const is = (x: any, y: any): boolean => 
  (x === y && (x !== 0 || 1 / x === 1 / y)) || (x !== x && y !== y) // eslint-disable-line no-self-compare

const objectIs: (x: any, y: any) => boolean = typeof Object.is === 'function' ? Object.is : is;

export default objectIs;
