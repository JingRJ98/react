# react-reconciler
react-reconciler包的主要作用, 将主要功能分为 4 个方面:
1. 输入: 暴露api函数(如: scheduleUpdateOnFiber), 供给其他包(如react包)调用.
2. 注册调度任务: 与调度中心(scheduler包)交互, 注册调度任务task, 等待任务回调.
3. 执行任务回调: 在内存中构造出fiber树, 同时与与渲染器(react-dom)交互, 在内存中创建出与fiber对应的DOM节点.
4. 输出: 与渲染器(react-dom)交互, 渲染DOM节点.

这四步可以反映react-reconciler包从输入到输出的运作流程,这是一个固定流程, 每一次更新都会运行.

在ReactFiberWorkLoop.js中, 承接输入的函数只有 scheduleUpdateOnFiber
在react-reconciler对外暴露的 api 函数中, 只要涉及到需要改变 fiber 的操作(无论是首次渲染或后续更新操作), 最后都会间接调用 scheduleUpdateOnFiber, 所以scheduleUpdateOnFiber函数是输入链路中的必经之路.


开发人员其实开发的每个组件(jsx语法)都会被编译器转换, 最终变成ReactElement
fiber对象是根据ReactElement对象构建出来的, 多个fiber对象构成fiber树
ReactElement树(不是严格的树结构, 为了方便也称为树)驱动fiber树.
fiber树是DOM树的数据模型, fiber树驱动DOM树

