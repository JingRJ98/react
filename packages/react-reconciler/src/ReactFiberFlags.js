/******************************* 副作用flags ***********************************/
export type Flags = number;

export const NoFlags = /*                      */ 0b00000000000000000000000000;
export const PerformedWork = /*                */ 0b00000000000000000000000001;

/**
 * 当前fiber节点或子孙fiber节点存在需要插入或移动的HostComponent或HostText
 */
export const Placement = /*                    */ 0b00000000000000000000000010;
/**
 * ClassComponent存在更新 且定义了componentDidMount或者componentDidUpdate方法;
 * HostComponent发生属性变化;
 * HostText发生变化;
 * FC定义了useLayoutEffect
 */
export const Update = /*                       */ 0b00000000000000000000000100;
export const Deletion = /*                     */ 0b00000000000000000000001000;
/** 有需要被删除的子HostComponent或子HostText */
export const ChildDeletion = /*                */ 0b00000000000000000000010000;
/** 清空HostComponent的文本内容 */
export const ContentReset = /*                 */ 0b00000000000000000000100000;
/**
 * 当ClassComponent中的this.setState执行时
 * 或ReactDOM.render执行时传递了回调函数参数
 */
export const Callback = /*                     */ 0b00000000000000000001000000;
export const DidCapture = /*                   */ 0b00000000000000000010000000;
export const ForceClientRender = /*            */ 0b00000000000000000100000000;
/** HostComponnt ref属性的创建与更新 */
export const Ref = /*                          */ 0b00000000000000001000000000;
/**
 * ClassComponent存在更新, 且定义了getSnapshotBeforeUpdate方法;
 */
export const Snapshot = /*                     */ 0b00000000000000010000000000;
/** FC中定义了useEffect且需要触发回调函数 */
export const Passive = /*                      */ 0b00000000000000100000000000;
/** Hydrating相关(核心用不到) */
export const Hydrating = /*                    */ 0b00000000000001000000000000;
/**
 * 控制SuspenseComponent的子树与fallbak切换时子树的显隐
 */
export const Visibility = /*                   */ 0b00000000000010000000000000;
export const StoreConsistency = /*             */ 0b00000000000100000000000000;

export const LifecycleEffectMask = /*             0b00000000000100111001000100 */
  Passive | Update | Callback | Ref | Snapshot | StoreConsistency;

// Union of all commit flags (flags with the lifetime of a particular commit)
export const HostEffectMask = /*               */ 0b00000000000111111111111111;

// These are not really side effects, but we still reuse this field.
export const Incomplete = /*                   */ 0b00000000001000000000000000;
export const ShouldCapture = /*                */ 0b00000000010000000000000000;
export const ForceUpdateForLegacySuspense = /* */ 0b00000000100000000000000000;
export const DidPropagateContext = /*          */ 0b00000001000000000000000000;
export const NeedsPropagation = /*             */ 0b00000010000000000000000000;
export const Forked = /*                       */ 0b00000100000000000000000000;

// Static tags describe aspects of a fiber that are not specific to a render,
// e.g. a fiber uses a passive effect (even if there are no updates on this particular render).
// This enables us to defer more work in the unmount case,
// since we can defer traversing the tree during layout to look for Passive effects,
// and instead rely on the static flag as a signal that there may be cleanup work.
export const RefStatic = /*                    */ 0b00001000000000000000000000;
export const LayoutStatic = /*                 */ 0b00010000000000000000000000;
export const PassiveStatic = /*                */ 0b00100000000000000000000000;

/** 0b00000000000000010000000100 */
export const BeforeMutationMask = Update | Snapshot

/**
 * 0b00000000000011001000110110
 * MutationMask相关的flags大多与ui相关
 * */
export const MutationMask =
  Placement |
  Update |
  ChildDeletion |
  ContentReset |
  Ref |
  Hydrating |
  Visibility;
/** 0b00000000000010001001000100 */
export const LayoutMask = Update | Callback | Ref | Visibility;

/** 0b00000000000000100000010000 */
export const PassiveMask = Passive | ChildDeletion;

// Union of tags that don't get reset on clones.
// This allows certain concepts to persist without recalculating them,
// e.g. whether a subtree contains passive effects or portals.
export const StaticMask = LayoutStatic | PassiveStatic | RefStatic; // 0b00111000000000000000000000
