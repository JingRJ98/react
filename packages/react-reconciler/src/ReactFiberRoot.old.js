import type {ReactNodeList} from 'shared/ReactTypes';
import type {
  Fiber,
  FiberRoot,
  SuspenseHydrationCallbacks,
  TransitionTracingCallbacks,
} from './ReactInternalTypes';
import type {RootTag} from './ReactRootTags';
import type {Cache} from './ReactFiberCacheComponent.old';
import type {
  PendingSuspenseBoundaries,
  Transition,
} from './ReactFiberTracingMarkerComponent.old';

import {noTimeout} from 'react-dom/src/client/ReactDOMHostConfig';
import {createHostRootFiber} from './ReactFiber.old';
import {
  NoLane,
  NoLanes,
  NoTimestamp,
  createLaneMap,
} from './ReactFiberLane.old';
import {initializeUpdateQueue} from './ReactFiberClassUpdateQueue.old';

export type RootState = {
  element: any,
  isDehydrated: boolean,
  cache: Cache,
  pendingSuspenseBoundaries: PendingSuspenseBoundaries | null,
  transitions: Set<Transition> | null,
};

function FiberRootNode(
  containerInfo,
  tag, // RootTag, // LegacyRoot | ConcurrentRoot 遗留 | 并发 root的type 该RootTag的类型决定了整个应用是否支持可中断渲染
  hydrate, // 这个参数没用
  identifierPrefix,
  onRecoverableError,
) {
  // 该RootTag的类型决定了整个应用是否支持可中断渲染
  this.tag = tag;
  // root节点 render方法接收的第二个参数
  this.containerInfo = containerInfo;
  // 只有持久化更新中会用到 也就是不支持增量更新的平台 react-dom不会用到
  this.pendingChildren = null;
  // 指向HostRootFiber
  this.current = null;
  this.pingCache = null;
  this.finishedWork = null;
  this.timeoutHandle = noTimeout;
  this.context = null;
  this.pendingContext = null;
  this.callbackNode = null;
  this.callbackPriority = NoLane;
  this.eventTimes = createLaneMap(NoLanes);
  this.expirationTimes = createLaneMap(NoTimestamp);

  /**
   * root.pengdingLanes与另外两个lanes配合紧密: fiberNode.lanes和fiberNode.childLanes
   * 后面两个lanes 一个表示fiber节点中待执行的lanes, 一个表示所有子孙中待执行的lanes
   * root.pengdingLanes和他们的关系是:
   * const remainingLanes = HostRootFiber.lanes | HotsRootFiber.childLanes
   * 从所有待执行的lanes中 移除 本次更新后剩下待执行的lanes 等于 本次更新中执行的lanes(pengdingLanes在本次更新中执行的lanes)
   * const noLongerPendingLanes  = root.pendingLanes & ~remainingLanes
   */
  this.pendingLanes = NoLanes; // pending
  this.suspendedLanes = NoLanes; // 异步组件相关
  this.pingedLanes = NoLanes; // 加载中
  this.expiredLanes = NoLanes; // 过期的
  this.mutableReadLanes = NoLanes; // 可变的
  this.finishedLanes = NoLanes; // 完成的

  this.entangledLanes = NoLanes; // 纠缠
  this.entanglements = createLaneMap(NoLanes);

  // useId中会使用, 但是一般不会在createRoot的时候通过参数传入, 所以是空字符串
  this.identifierPrefix = identifierPrefix;
  this.onRecoverableError = onRecoverableError;
}

export function createFiberRoot(
  containerInfo: any,
  tag: RootTag, // LegacyRoot | ConcurrentRoot 遗留 | 并发 root的type 该RootTag的类型决定了整个应用是否支持可中断渲染
  hydrate: boolean, // false
  initialChildren: ReactNodeList,
  hydrationCallbacks: null | SuspenseHydrationCallbacks,
  isStrictMode: boolean,
  concurrentUpdatesByDefaultOverride: null | boolean,
  identifierPrefix: string,
  onRecoverableError: null | ((error: mixed) => void),
  transitionCallbacks: null | TransitionTracingCallbacks,
): FiberRoot {
  const root: FiberRoot = new FiberRootNode(
    containerInfo,
    tag,
    false,
    identifierPrefix,
    onRecoverableError,
  );

  // createHostRootFiber返回这个fiber节点直接挂载根dom元素下, 所以也叫 rootFiber
  const uninitializedFiber: Fiber = createHostRootFiber(
    tag,
    isStrictMode,
    concurrentUpdatesByDefaultOverride,
  );
  // 将两者互相关联
  // FiberRoot.current => rootFiberNode
  root.current = uninitializedFiber;
  // rootFiberNode.stateNode => FiberRoot
  uninitializedFiber.stateNode = root;

  const initialState: RootState = {
    element: initialChildren,
    isDehydrated: false,
    cache: null, // not enabled yet
    transitions: null,
    pendingSuspenseBoundaries: null,
  };
  uninitializedFiber.memoizedState = initialState;

  // 初始化 uninitializedFiber
  initializeUpdateQueue(uninitializedFiber);

  return root;
}
