export const LegacyRoot = 0;
export const ConcurrentRoot = 1;

export type RootTag = 0 | 1;
