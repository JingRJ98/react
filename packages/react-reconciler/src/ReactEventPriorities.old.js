import type {Lane, Lanes} from './ReactFiberLane.old';

import {
  NoLane,
  SyncLane,
  InputContinuousLane,
  DefaultLane,
  IdleLane,
  getHighestPriorityLane,
  includesNonIdleWork,
} from './ReactFiberLane.old';

export type EventPriority = Lane;

/**
 * 离散事件优先级 (同步车道 0b0000000000000000000000000000001)
 * 比如click input focus blur
 * */
export const DiscreteEventPriority: EventPriority = SyncLane;
/**
 * 连续事件优先级 (连续输入车道 0b0000000000000000000000000000100)
 * 比如drag scroll mousemove
 * */
export const ContinuousEventPriority: EventPriority = InputContinuousLane;
/** 默认事件优先级 (默认车道 0b0000000000000000000000000010000) */
export const DefaultEventPriority: EventPriority = DefaultLane;
/** 闲置事件优先级 (限制车道 0b0100000000000000000000000000000)*/
export const IdleEventPriority: EventPriority = IdleLane;

let currentUpdatePriority: EventPriority = NoLane; // 全0

export function getCurrentUpdatePriority(): EventPriority {
  return currentUpdatePriority;
}

export function setCurrentUpdatePriority(newPriority: EventPriority) {
  currentUpdatePriority = newPriority;
}

/**
 * 手动设置优先级
 * 手动设置完成之后 在回调函数的执行期间 通过getCurrentUpdatePriori获取手动设置的优先级
 * */
export function runWithPriority<T>(priority: EventPriority, fn: () => T): T {
  const previousPriority = currentUpdatePriority;
  try {
    currentUpdatePriority = priority;
    // 回调函数在执行期间, 可以通过getCurrentUpdatePriorit函数获得现在手动设置的优先级
    return fn();
  } finally {
    // 回调函数执行完成之后 重置优先级
    currentUpdatePriority = previousPriority;
  }
}

/**
 * a和b中权限更高的(a不为0, 全0是最低权限)
 */
export function higherEventPriority(
  a: EventPriority,
  b: EventPriority,
): EventPriority {
  return a !== 0 && a < b ? a : b;
}

/**
 * a和b中权限更低的, 全0是最低权限
 */
export function lowerEventPriority(
  a: EventPriority,
  b: EventPriority,
): EventPriority {
  return a === 0 || a > b ? a : b;
}

/**
 * 是否a权限比b权限更高(a不为0) 越小的lane, 1越在高位
 */
export const isHigherEventPriority = (
  a: EventPriority,
  b: EventPriority,
): boolean => a !== 0 && a < b;

/**
 * lanes转换为事件优先级
 * React lane转schedule优先级的第一步
 */
export function lanesToEventPriority(lanes: Lanes): EventPriority {
  const lane = getHighestPriorityLane(lanes);
  if (!isHigherEventPriority(DiscreteEventPriority, lane)) {
    // 如果lanes中最高的lane 已经达到或超过同步优先级, 返回同步优先级
    // 实际上是不可能的, 因为同步事件优先级是1
    return DiscreteEventPriority;
  }
  if (!isHigherEventPriority(ContinuousEventPriority, lane)) {
    // 如果lanes中最高的lane 已经达到或超过连续事件优先级, 返回连续事件优先级
    return ContinuousEventPriority;
  }
  if (includesNonIdleWork(lane)) {
    // 包含非Idle的任务, 返回默认优先级
    return DefaultEventPriority;
  }
  return IdleEventPriority;
}
