import type {Fiber} from './ReactInternalTypes';
import type {OffscreenInstance} from './ReactFiberOffscreenComponent';

type SuspenseInfo = {name: string | null};

export type Transition = {
  name: string,
  startTime: number,
};

export type BatchConfigTransition = {
  name?: string,
  startTime?: number,
  _updatedFibers?: Set<Fiber>,
};

export type PendingSuspenseBoundaries = Map<OffscreenInstance, SuspenseInfo>;

