import type {
  RefObject,
  ReactContext,
  StartTransitionOptions,
  Wakeable,
} from 'shared/ReactTypes';
import type {SuspenseInstance} from 'react-dom/src/client/ReactDOMHostConfig';
import type {WorkTag} from './ReactWorkTags';
import type {TypeOfMode} from './ReactTypeOfMode';
import type {Flags} from './ReactFiberFlags';
import type {Lane, Lanes, LaneMap} from './ReactFiberLane.old';
import type {RootTag} from './ReactRootTags';
import type {TimeoutHandle, NoTimeout} from 'react-dom/src/client/ReactDOMHostConfig';
import type {Cache} from './ReactFiberCacheComponent.old';
import type {Transition} from './ReactFiberTracingMarkerComponent.old';
import type {ConcurrentUpdate} from './ReactFiberConcurrentUpdates.new';

export type ContextDependency<T> = {
  context: ReactContext<T>,
  next: ContextDependency<mixed> | null,
  memoizedValue: T,
  ...
};

export type Dependencies = {
  lanes: Lanes,
  firstContext: ContextDependency<mixed> | null,
  ...
};

// 一个Fiber对象代表一个即将渲染或者已经渲染的组件(ReactElement), 一个组件可能对应两个fiber(current和WorkInProgress)
export type Fiber = {
  // 实例在组件的所有版本之间共享。 我们可以轻松地将其分解为一个单独的对象，以避免将太多内容复制到树的备用版本。
  // 我们现在将其放在单个对象上，以最大限度地减少初始渲染期间创建的对象数量。

  /** 标记不同的组件类型 根据ReactElement的 type 生成 数字0-25 */
  tag: WorkTag,

  /** 组件的唯一标识 和ReactElement组件的 key 一致 */
  key: null | string,

  /** 一般来讲和ReactElement组件的 type 一致 */
  elementType: any,

  /**
   * 一般来讲和fiber.elementType一致.
   * 一些特殊情形下, 比如在开发环境下为了兼容热更新(HotReloading), 会对function, class, ForwardRef类型的ReactElement做一定的处理, 这种情况会区别于fiber.elementType
   * 异步组件resolved之后返回的内容, 一般是'function' 或者'class'
   * */
  type: any,

  /** 跟当前Fiber相关的状态节点, 比如rootFiber的状态节点是FiberRoot */
  stateNode: any,

  // 指向当前节点的父节点, 用来在处理完这个节点之后向上返回
  return: Fiber | null,
  // 单链表树结构
  // 指向自己的第一个子节点
  child: Fiber | null,
  /**
   * 指向自己的兄弟节点
   * 所有兄弟节点的return都指向同一个父节点
   */
  sibling: Fiber | null,
  /** fiber 在兄弟节点中的索引, 如果是单节点默认为 0. */
  index: number,

  // The ref last used to attach this node.
  // I'll avoid adding an owner field for prod and model that as functions.
  ref:
    | null
    | (((handle: mixed) => void) & {_stringRef: ?string, ...})
    | RefObject,

  /** 从ReactElement传入的 props. 用于和`fiber.memoizedProps`比较可以得出属性是否变动 */
  pendingProps: any,
  /** 上一次渲染完成之后的props */
  memoizedProps: any,

  /** 该Fiber对应的组件产生的Update会放在这个队列里面
   *  当前节点的state改动之后, 都会创建一个update对象添加到这个队列中
   * */
  updateQueue: mixed,

  /** 用于输出的state, 最终渲染所使用的state */
  memoizedState: any,

  /** 该fiber节点所依赖的(contexts, events)等 */
  dependencies: Dependencies | null,

  // 影响本fiber节点及其子树中所有节点
  // ConcurrentMode共存模式表示这个子树是否默认是异步渲染的
  // Fiber被创建的时候会继承父Fiber的mode
  // 其他标识也可以在创建Fiber的时候设置, 但是创建之后不应该再被修改,特别是子Fiber创建之前
  mode: TypeOfMode,

  // Effect
  flags: Flags,
  /** 替代16.x版本中的 firstEffect, nextEffect. */
  subtreeFlags: Flags,
  /** 存储将要被删除的子节点. */
  deletions: Array<Fiber> | null,

  /** 单向链表, 指向下一个有副作用的fiber节点 */
  nextEffect: Fiber | null,

  // 副作用链表中的第一个fiber节点和副作用链表中的最后一个fiber节点
  // 这允许我们在重用当前Fiber中完成的工作时重用链表的一部分。
  firstEffect: Fiber | null,
  lastEffect: Fiber | null,

  // 优先级相关
  /** 本fiber节点的优先级 */
  lanes: Lanes,
  /** 子节点的优先级 */
  childLanes: Lanes,

  // 指针
  // Fiber树更新的过程中, 每个Fiber都会有一个跟其对应的Fiber (双缓存)
  // current <=> workInProgress
  // 渲染完成后 他们会交换位置
  alternate: Fiber | null,
};

type BaseFiberRootProperties = {
  /** LegacyRoot | ConcurrentRoot, 即 0 | 1 */
  tag: RootTag,

  // render方法接收的参数, 即App组件
  containerInfo: any,
  // 只有在持久更新中使用, react-dom中不会使用
  pendingChildren: any,
  // FiberRoot.current指向 rootFiber.
  current: Fiber,

  pingCache: WeakMap<Wakeable, Set<mixed>> | Map<Wakeable, Set<mixed>> | null,

  /** 记录在一次更新过程中完成了的任务 */
  finishedWork: Fiber | null,
  /** 在任务被挂起时通过setTimeout设置的返回内容 用来下一次如果有新的任务挂起时清理还没触发的timeout */
  timeoutHandle: TimeoutHandle | NoTimeout,
  /** 顶层context对象 只有主动调用renderSubtreeIntoContainer时才会有用 */
  context: Object | null,
  pendingContext: Object | null,

  // Node returned by Scheduler.scheduleCallback. Represents the next rendering
  // task that the root will work on.
  callbackNode: *,
  callbackPriority: Lane,
  eventTimes: LaneMap<number>,
  /** 更新对应的国企时间 */
  expirationTimes: LaneMap<number>,
  hiddenUpdates: LaneMap<Array<ConcurrentUpdate> | null>,

  pendingLanes: Lanes,
  suspendedLanes: Lanes,
  pingedLanes: Lanes,
  expiredLanes: Lanes,
  mutableReadLanes: Lanes,

  finishedLanes: Lanes,

  entangledLanes: Lanes,
  entanglements: LaneMap<Lanes>,

  pooledCache: Cache | null,
  pooledCacheLanes: Lanes,

  // 用于useId, 也可传入参数进行配置
  identifierPrefix: string,

  onRecoverableError: (
    error: mixed,
    errorInfo: {digest?: ?string, componentStack?: ?string},
  ) => void,
};

// The following attributes are only used by DevTools and are only present in DEV builds.
// They enable DevTools Profiler UI to show which Fiber(s) scheduled a given commit.
type UpdaterTrackingOnlyFiberRootProperties = {|
  memoizedUpdaters: Set<Fiber>,
  pendingUpdatersLaneMap: LaneMap<Set<Fiber>>,
|};

export type SuspenseHydrationCallbacks = {
  onHydrated?: (suspenseInstance: SuspenseInstance) => void,
  onDeleted?: (suspenseInstance: SuspenseInstance) => void,
  ...
};

export type TransitionTracingCallbacks = {
  onTransitionStart?: (transitionName: string, startTime: number) => void,
  onTransitionProgress?: (
    transitionName: string,
    startTime: number,
    currentTime: number,
    pending: Array<{name: null | string}>,
  ) => void,
  onTransitionIncomplete?: (
    transitionName: string,
    startTime: number,
    deletions: Array<{
      type: string,
      name?: string,
      newName?: string,
      endTime: number,
    }>,
  ) => void,
  onTransitionComplete?: (
    transitionName: string,
    startTime: number,
    endTime: number,
  ) => void,
  onMarkerProgress?: (
    transitionName: string,
    marker: string,
    startTime: number,
    currentTime: number,
    pending: Array<{name: null | string}>,
  ) => void,
  onMarkerIncomplete?: (
    transitionName: string,
    marker: string,
    startTime: number,
    deletions: Array<{
      type: string,
      name?: string,
      newName?: string,
      endTime: number,
    }>,
  ) => void,
  onMarkerComplete?: (
    transitionName: string,
    marker: string,
    startTime: number,
    endTime: number,
  ) => void,
};

// The following fields are only used in transition tracing in Profile builds
type TransitionTracingOnlyFiberRootProperties = {|
  transitionCallbacks: null | TransitionTracingCallbacks,
  transitionLanes: Array<Array<Transition> | null>,
|};

// Exported FiberRoot type includes all properties,
// To avoid requiring potentially error-prone :any casts throughout the project.
// The types are defined separately within this file to ensure they stay in sync.
export type FiberRoot = {
  ...BaseFiberRootProperties,
  ...UpdaterTrackingOnlyFiberRootProperties,
  ...TransitionTracingOnlyFiberRootProperties,
  ...
};

type BasicStateAction<S> = (S => S) | S;
type Dispatch<A> = A => void;

export type Dispatcher = {
  readContext<T>(context: ReactContext<T>): T,
  useState<S>(initialState: (() => S) | S): [S, Dispatch<BasicStateAction<S>>],
  useReducer<S, I, A>(
    reducer: (S, A) => S,
    initialArg: I,
    init?: (I) => S,
  ): [S, Dispatch<A>],
  useContext<T>(context: ReactContext<T>): T,
  useRef<T>(initialValue: T): {|current: T|},
  useEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void,
  useInsertionEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void,
  useLayoutEffect(
    create: () => (() => void) | void,
    deps: Array<mixed> | void | null,
  ): void,
  useCallback<T>(callback: T, deps: Array<mixed> | void | null): T,
  useMemo<T>(nextCreate: () => T, deps: Array<mixed> | void | null): T,
  useImperativeHandle<T>(
    ref: {|current: T | null|} | ((inst: T | null) => mixed) | null | void,
    create: () => T,
    deps: Array<mixed> | void | null,
  ): void,
  useDeferredValue<T>(value: T): T,
  useTransition(): [
    boolean,
    (callback: () => void, options?: StartTransitionOptions) => void,
  ],
  useId(): string,

  unstable_isNewReconciler?: boolean,
};
