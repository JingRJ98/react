import type { Fiber } from './ReactInternalTypes';
import type { CapturedValue } from './ReactCapturedValue';

export function logCapturedError(
  boundary: Fiber,
  errorInfo: CapturedValue<mixed>,
): void {
  try {
    const error = (errorInfo.value: any);
    console['error'](error);
  } catch (e) {
    // 防止console.error被覆盖报错
    setTimeout(() => {
      throw e;
    });
  }
}
