/* eslint-disable react-internal/prod-error-codes */
import type { Fiber } from './ReactInternalTypes';
import type { FiberRoot } from './ReactInternalTypes';
import type { Instance } from 'react-dom/src/client/ReactDOMHostConfig';
import type { ReactNodeList } from 'shared/ReactTypes';

export type Family = {|
  current: any,
|};

export type RefreshUpdate = {|
  staleFamilies: Set < Family >,
    updatedFamilies: Set < Family >,
|};

// Resolves type to a family.
type RefreshHandler = any => Family | void;

// Used by React Refresh runtime through DevTools Global Hook.
export type SetRefreshHandler = (handler: RefreshHandler | null) => void;
export type ScheduleRefresh = (root: FiberRoot, update: RefreshUpdate) => void;
export type ScheduleRoot = (root: FiberRoot, element: ReactNodeList) => void;
export type FindHostInstancesForRefresh = (
  root: FiberRoot,
  families: Array<Family>,
) => Set<Instance>;

export const setRefreshHandler = (handler: RefreshHandler | null): void => {

};

export function resolveFunctionForHotReloading(type: any): any {
  return type;
}

export function resolveClassForHotReloading(type: any): any {
  // No implementation differences.
  return resolveFunctionForHotReloading(type);
}

export function resolveForwardRefForHotReloading(type: any): any {
  return type;
}

export function markFailedErrorBoundaryForHotReloading(fiber: Fiber) {
}

export const scheduleRefresh: ScheduleRefresh = (
  root: FiberRoot,
  update: RefreshUpdate,
): void => {
};

export const scheduleRoot: ScheduleRoot = (
  root: FiberRoot,
  element: ReactNodeList,
): void => {
};

export const findHostInstancesForRefresh: FindHostInstancesForRefresh = (
  root: FiberRoot,
  families: Array<Family>,
): Set<Instance> => {
  throw new Error(
    'Did not expect findHostInstancesForRefresh to be called in production.',
  );
};
