// Math.clz32() 函数返回一个数字在转换成 32 无符号整形数字的二进制形式后，左边的 0 的个数
// 比如 1000000 转换成 32 位无符号整形数字的二进制形式后是 0000 0000 0000 1111 0100 0010 0100 0000
// 开头的 0 的个数是 12 个，则 Math.clz32(1000000) 返回 12.
// Based on: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32
export const clz32 = Math.clz32 ? Math.clz32 : clz32Fallback;

const log = Math.log;
const LN2 = Math.LN2;
function clz32Fallback(x: number): number {
  const asUint = x >>> 0;
  if (asUint === 0) {
    return 32;
  }
  // Math.log是 10为底的对数
  // (log(asUint) / LN2) | 0 是小于等于asUint的2**n里面 的n
  // (log(4) / LN2) | 0 = 2, 所以转32位无符号二进制数后, 前面有29个0, 0111, 最后要占据n + 1个
  // (log(7) / LN2) | 0 = 2, 所以转32位无符号二进制数后, 前面有29个0, 0100, 最后要占据n + 1个
  // (log(8) / LN2) | 0 = 3, 所以转32位无符号二进制数后, 前面有28个0, 1000, 最后要占据n + 1个
  // 所以用31 减而不是32
  return (31 - ((log(asUint) / LN2) | 0)) | 0;
}
