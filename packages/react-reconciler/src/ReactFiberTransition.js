import ReactSharedInternals from 'react/src/ReactSharedInternals';
import type {Transition} from './ReactFiberTracingMarkerComponent.old';

const {ReactCurrentBatchConfig} = ReactSharedInternals;

/**
 * return null
 */
export function requestCurrentTransition(): Transition | null {
  return ReactCurrentBatchConfig.transition;
}
