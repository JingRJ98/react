export type HookFlags = number;

export const NoFlags = /*    */ 0b0000;

// Represents whether effect should fire.
export const HasEffect = /*  */ 0b0001;

// useInsertionEffect
export const Insertion = /*  */ 0b0010;
// useLayoutEffect
export const Layout = /*     */ 0b0100;
// useEffect
export const Passive = /*    */ 0b1000;
