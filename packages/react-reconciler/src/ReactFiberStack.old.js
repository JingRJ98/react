import type { Fiber } from './ReactInternalTypes';

export type StackCursor<T> = { current: T };

const valueStack: Array<any> = [];

let index = -1;

const createCursor = <T,>(defaultValue: T): StackCursor<T> => ({
  current: defaultValue,
})

const isEmpty = (): boolean => index === -1;

const pop = <T,>(cursor: StackCursor<T>, fiber: Fiber): void => {
  if (index < 0) return;

  cursor.current = valueStack[index];
  valueStack[index--] = null;
}

const push = <T,>(cursor: StackCursor<T>, value: T, fiber: Fiber): void => {
  valueStack[++index] = cursor.current;
  cursor.current = value;
}

export {
  createCursor,
  isEmpty,
  pop,
  push,
};
