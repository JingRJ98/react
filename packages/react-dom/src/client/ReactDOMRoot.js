import type {ReactNodeList} from 'shared/ReactTypes';
import type {
  FiberRoot,
  TransitionTracingCallbacks,
} from 'react-reconciler/src/ReactInternalTypes';

export type RootType = {
  render(children: ReactNodeList): void,
  unmount(): void,
  _internalRoot: FiberRoot | null,
  ...
};

export type CreateRootOptions = {
  unstable_strictMode?: boolean,
  unstable_concurrentUpdatesByDefault?: boolean,
  identifierPrefix?: string,
  onRecoverableError?: (error: mixed) => void,
  transitionCallbacks?: TransitionTracingCallbacks,
};

import {
  markContainerAsRoot,
  unmarkContainerAsRoot,
} from './ReactDOMComponentTree';
import {listenToAllSupportedEvents} from '../events/DOMPluginEventSystem';
import { COMMENT_NODE } from '../shared/HTMLNodeType';
import {
  createContainer,
  updateContainer,
  flushSync,
} from 'react-reconciler/src/ReactFiberReconciler';
import {ConcurrentRoot} from 'react-reconciler/src/ReactRootTags';

/* global reportError */
const defaultOnRecoverableError =
  typeof reportError === 'function'
    ? // In modern browsers, reportError will dispatch an error event,
      // emulating an uncaught JavaScript error.
      reportError
    : (error: mixed) => {
        // In older browsers and test environments, fallback to console.error.
        console['error'](error);
      };

/**
 * ReactDOMRoot 构造函数, 接收参数为FiberRoot
 * 只有两个方法 render unmount
 * @param {FiberRoot} internalRoot
 */
function ReactDOMRoot(internalRoot: FiberRoot) {
  // FiberRoot
  this._internalRoot = internalRoot;
}

ReactDOMRoot.prototype.render = function(
  children: ReactNodeList, // 一般是render的根组件<App />, 或者被包裹的根组件
): void {
  const root = this._internalRoot; // FiberRoot

  updateContainer(children, root, null, null);
};

ReactDOMRoot.prototype.unmount = function(): void {
  const root = this._internalRoot;
  if (root !== null) {
    this._internalRoot = null;
    const container = root.containerInfo;
    flushSync(() => {
      updateContainer(null, root, null, null);
    });
    unmarkContainerAsRoot(container);
  }
};

export function createRoot(
  container: Element | Document | DocumentFragment,
  options?: CreateRootOptions,
): RootType {
  let isStrictMode = false;
  const concurrentUpdatesByDefaultOverride = false;
  let identifierPrefix = ''; // useId中会使用
  let onRecoverableError = defaultOnRecoverableError;
  let transitionCallbacks = null;

  if (options !== null && options !== undefined) {
    if (options.unstable_strictMode === true) {
      isStrictMode = true;
    }
    if (options.identifierPrefix !== undefined) {
      identifierPrefix = options.identifierPrefix;
    }
    if (options.onRecoverableError !== undefined) {
      onRecoverableError = options.onRecoverableError;
    }
    if (options.transitionCallbacks !== undefined) {
      transitionCallbacks = options.transitionCallbacks;
    }
  }

  // 创建容器对象 返回 FiberRoot
  const root = createContainer(
    container,
    ConcurrentRoot,
    null,
    isStrictMode,
    concurrentUpdatesByDefaultOverride,
    identifierPrefix,
    onRecoverableError,
    transitionCallbacks,
  );

  // root是fiberRoot
  // root.current指向的是fiberRoot下面挂载的Fiber节点(注意是.current而不是自己)
  // 把这个 fiber节点 (可能会叫rootFiber)挂载到dom根元素的'__reactContainer$随机字符串'属性上面
  markContainerAsRoot(root.current, container);

  const rootContainerElement: Document | Element | DocumentFragment =
  // Comment 节点表示注释节点
  // 尽管它通常不会显示出来，但是在查看源码时可以看到它们。
  // 在 HTML 和 XML 里，注释（Comments）为 '<!--' 和 '-->' 之间的内容
    container.nodeType === COMMENT_NODE
      // 如果container是注释节点, dom根元素设置为它的父元素节点
      ? (container.parentNode: any)
      : container;

  // react合成事件机制, 将事件挂载到根dom元素上
  listenToAllSupportedEvents(rootContainerElement);

  return new ReactDOMRoot(root);
}
