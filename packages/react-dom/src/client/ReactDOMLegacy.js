import type { Container } from './ReactDOMHostConfig';
import type { FiberRoot } from 'react-reconciler/src/ReactInternalTypes';
import type { ReactNodeList } from 'shared/ReactTypes';

import {
  markContainerAsRoot,
  unmarkContainerAsRoot,
} from './ReactDOMComponentTree';
import { listenToAllSupportedEvents } from '../events/DOMPluginEventSystem';
import {
  ELEMENT_NODE,
  COMMENT_NODE,
} from '../shared/HTMLNodeType';

import {
  createContainer,
  updateContainer,
  flushSync,
  getPublicRootInstance,
  findHostInstance,
} from 'react-reconciler/src/ReactFiberReconciler';
import { LegacyRoot } from 'react-reconciler/src/ReactRootTags';
import { has as hasInstance } from 'shared/ReactInstanceMap';

function noopOnRecoverableError() {
  // This isn't reachable because onRecoverableError isn't called in the
  // legacy API.
}

function legacyCreateRootFromDOMContainer(
  container: Container,
  initialChildren: ReactNodeList,
  parentComponent: ?React$Component<any, any>,
  callback: ?Function,
  isHydrationContainer: boolean, // false
): FiberRoot {
  // First clear any existing content.
  let rootSibling;
  while ((rootSibling = container.lastChild)) {
    container.removeChild(rootSibling);
  }

  if (typeof callback === 'function') {
    const originalCallback = callback;
    callback = function () {
      const instance = getPublicRootInstance(root);
      originalCallback.call(instance);
    };
  }

  // 创建一个FiberRoot 参考./ReactDOMRoot.js中的createContainer
  const root = createContainer(
    container,
    LegacyRoot,
    null, // hydrationCallbacks
    false, // isStrictMode
    false, // concurrentUpdatesByDefaultOverride,
    '', // identifierPrefix
    noopOnRecoverableError, // onRecoverableError
    null, // transitionCallbacks
  );
  container._reactRootContainer = root;
  // root是fiberRoot
  // root.current指向的是fiberRoot下面挂载的Fiber节点(注意是.current而不是自己)
  // 把这个 fiber节点 (可能会叫rootFiber)挂载到dom根元素的'__reactContainer$随机字符串'属性上面
  // 标记完成之后
  markContainerAsRoot(root.current, container);

  const rootContainerElement = container.nodeType === COMMENT_NODE ? container.parentNode : container;

  // react合成事件机制, 将事件挂载到根dom元素上
  listenToAllSupportedEvents(rootContainerElement);

  flushSync(() => {
    updateContainer(initialChildren, root, parentComponent, callback);
  });

  return root;
}

function legacyRenderSubtreeIntoContainer(
  // null
  parentComponent: ?React$Component<any, any>,
  // <App /> 根组件
  children: ReactNodeList,
  // 容器
  container: Container,
  // false
  forceHydrate: boolean,
  // undefined
  callback: ?Function,
) {
  const maybeRoot = container._reactRootContainer;
  let root: FiberRoot;
  if (!maybeRoot) {
    // Initial mount
    root = legacyCreateRootFromDOMContainer(
      container,
      children,
      parentComponent,
      callback,
      false,
    );
  } else {
    root = maybeRoot;
    if (typeof callback === 'function') {
      const originalCallback = callback;
      callback = function () {
        const instance = getPublicRootInstance(root);
        originalCallback.call(instance);
      };
    }
    // Update
    updateContainer(children, root, parentComponent, callback);
  }
  return getPublicRootInstance(root);
}

export function findDOMNode(
  componentOrElement: Element | ?React$Component<any, any>,
): null | Element | Text {
  if (componentOrElement == null) {
    return null;
  }
  if ((componentOrElement: any).nodeType === ELEMENT_NODE) {
    return (componentOrElement: any);
  }
  return findHostInstance(componentOrElement);
}

export function render(
  // <App/>
  element: React$Element<any>,
  // dom
  container: Container,
  callback: ?Function,
) {
  return legacyRenderSubtreeIntoContainer(
    null,
    element,
    container,
    false,
    callback,
  );
}

export function unstable_renderSubtreeIntoContainer(
  parentComponent: React$Component<any, any>,
  element: React$Element<any>,
  containerNode: Container,
  callback: ?Function,
) {
  if (parentComponent == null || !hasInstance(parentComponent)) {
    // ('parentComponent must be a valid React Component');
  }

  return legacyRenderSubtreeIntoContainer(
    parentComponent,
    element,
    containerNode,
    false,
    callback,
  );
}

export function unmountComponentAtNode(container: Container) {
  if (container._reactRootContainer) {
    // Unmount should not be batched.
    flushSync(() => {
      legacyRenderSubtreeIntoContainer(null, null, container, false, () => {
        // $FlowFixMe This should probably use `delete container._reactRootContainer`
        container._reactRootContainer = null;
        unmarkContainerAsRoot(container);
      });
    });
    // If you call unmountComponentAtNode twice in quick succession, you'll
    // get `true` twice. That's probably fine?
    return true;
  } else {
    return false;
  }
}
