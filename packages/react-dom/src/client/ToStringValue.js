export opaque type ToStringValue =
  | boolean
  | number
  | Object
  | string
  | null
  | void;


export function toString(value: ToStringValue): string {
  return `${value}`
}

export function getToStringValue(value: mixed): ToStringValue {
  switch (typeof value) {
    case 'boolean':
    case 'number':
    case 'string':
    case 'undefined':
    case 'object':
      return value;
    default:
      return '';
  }
}
