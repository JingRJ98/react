import type {DOMEventName} from './DOMEventNames';

// allNativeEvents中存入全小写的原生事件名
export const allNativeEvents: Set<DOMEventName> = new Set();
/**
 * Mapping from registration name to event name
 */
export const registrationNameDependencies = {};

export function registerTwoPhaseEvent(
  registrationName: string, // react中的合成事件名 (onClick, onContextMenu)
  dependencies: Array<DOMEventName>, // 全小写原生事件名数组, [click, contextmenu]
): void {
  // 冒泡阶段
  // onClick, ['click']
  registerDirectEvent(registrationName, dependencies);
  // 捕获阶段
  // onClickCapture, ['click']
  registerDirectEvent(registrationName + 'Capture', dependencies);
}

export function registerDirectEvent(
  registrationName: string,
  dependencies: Array<DOMEventName>,
) {
  // {
  //   onClick: ['click'],
  //   onClickCapture: ['click'],
  // }
  registrationNameDependencies[registrationName] = dependencies;

  for (let i = 0; i < dependencies.length; i++) {
    // allNativeEvents中存入全小写的原生事件名
    allNativeEvents.add(dependencies[i]);
  }
}
