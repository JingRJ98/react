import type { AnyNativeEvent } from '../events/PluginModuleType';
import type { FiberRoot } from 'react-reconciler/src/ReactInternalTypes';
import type { Container, SuspenseInstance } from '../client/ReactDOMHostConfig';
import type { DOMEventName } from '../events/DOMEventNames';
import {
  clearIfContinuousEvent,
  queueIfContinuousEvent,
  attemptSynchronousHydration,
} from './ReactDOMEventReplaying';
import {
  getNearestMountedFiber,
  getContainerFromFiber,
  getSuspenseInstanceFromFiber,
} from 'react-reconciler/src/ReactFiberTreeReflection';
import { HostRoot, SuspenseComponent } from 'react-reconciler/src/ReactWorkTags';
import { type EventSystemFlags, IS_CAPTURE_PHASE } from './EventSystemFlags';

import getEventTarget from './getEventTarget';
import {
  getInstanceFromNode,
  getClosestInstanceFromNode,
} from '../client/ReactDOMComponentTree';

import { dispatchEventForPluginEventSystem } from './DOMPluginEventSystem';

import {
  getCurrentPriorityLevel as getCurrentSchedulerPriorityLevel,
  IdlePriority as IdleSchedulerPriority,
  ImmediatePriority as ImmediateSchedulerPriority,
  LowPriority as LowSchedulerPriority,
  NormalPriority as NormalSchedulerPriority,
  UserBlockingPriority as UserBlockingSchedulerPriority,
} from 'react-reconciler/src/Scheduler';
import {
  DiscreteEventPriority, // 离散事件优先级
  ContinuousEventPriority,
  DefaultEventPriority,
  IdleEventPriority,
  getCurrentUpdatePriority,
  setCurrentUpdatePriority,
} from 'react-reconciler/src/ReactEventPriorities.old';
import ReactSharedInternals from 'react/src/ReactSharedInternals';
import { isRootDehydrated } from 'react-reconciler/src/ReactFiberShellHydration';

const { ReactCurrentBatchConfig } = ReactSharedInternals;

let _enabled = true;

export function setEnabled(enabled: ?boolean) {
  _enabled = !!enabled;
}

export function isEnabled() {
  return _enabled;
}

export function createEventListenerWrapper(
  targetContainer: EventTarget, // 委托的目标元素 根元素dom
  domEventName: DOMEventName, // 小写的原生事件名
  eventSystemFlags: EventSystemFlags, // 捕获阶段触发: 4 | 冒泡阶段触发: 0
): Function {
  return dispatchEvent.bind(
    null,
    domEventName,
    eventSystemFlags,
    targetContainer,
  );
}

export function createEventListenerWrapperWithPriority(
  targetContainer: EventTarget, // 委托的目标元素 根元素dom
  domEventName: DOMEventName, // 小写的原生事件名
  eventSystemFlags: EventSystemFlags, // 捕获阶段触发: 4 | 冒泡阶段触发: 0
): Function {
  // 根据不同的事件获取优先级(离散事件 连续事件 默认等)
  const eventPriority = getEventPriority(domEventName);
  let listenerWrapper;
  switch (eventPriority) {
    // 离散事件优先级 0b0000000000000000000000000000001;
    case DiscreteEventPriority:
      listenerWrapper = dispatchDiscreteEvent;
      break;
    // 连续事件优先级 0b0000000000000000000000000000100;
    case ContinuousEventPriority:
      listenerWrapper = dispatchContinuousEvent;
      break;
    // 默认事件优先级 0b0000000000000000000000000010000;
    case DefaultEventPriority:
    default:
      listenerWrapper = dispatchEvent;
      break;
  }
  return listenerWrapper.bind(
    null,
    domEventName,
    eventSystemFlags,
    targetContainer,
  );
}

function dispatchDiscreteEvent(
  domEventName, // 小写的原生事件名
  eventSystemFlags,// 捕获阶段触发: 4 | 冒泡阶段触发: 0
  container, // 委托的目标元素 根元素dom | document(只有文字选中事件)
  nativeEvent,
) {
  const previousPriority = getCurrentUpdatePriority();
  const prevTransition = ReactCurrentBatchConfig.transition;
  ReactCurrentBatchConfig.transition = null;
  try {
    // 设置离散更新优先级 0b0000000000000000000000000000001;
    setCurrentUpdatePriority(DiscreteEventPriority);
    dispatchEvent(domEventName, eventSystemFlags, container, nativeEvent);
  } finally {
    // 重置优先级
    setCurrentUpdatePriority(previousPriority);
    ReactCurrentBatchConfig.transition = prevTransition;
  }
}

function dispatchContinuousEvent(
  domEventName, // 小写的原生事件名
  eventSystemFlags,// 捕获阶段触发: 4 | 冒泡阶段触发: 0
  container,
  nativeEvent,
) {
  const previousPriority = getCurrentUpdatePriority();
  const prevTransition = ReactCurrentBatchConfig.transition;
  ReactCurrentBatchConfig.transition = null;
  try {
    // 设置连续事件优先级 0b0000000000000000000000000000100;
    setCurrentUpdatePriority(ContinuousEventPriority);
    dispatchEvent(domEventName, eventSystemFlags, container, nativeEvent);
  } finally {
    // 重置优先级
    setCurrentUpdatePriority(previousPriority);
    ReactCurrentBatchConfig.transition = prevTransition;
  }
}

export function dispatchEvent(
  domEventName: DOMEventName, // 小写的原生事件名
  eventSystemFlags: EventSystemFlags, // 捕获阶段触发: 4 | 冒泡阶段触发: 0
  targetContainer: EventTarget, // 委托的目标元素 根元素dom
  nativeEvent: AnyNativeEvent,
): void {
  if (!_enabled) {
    return;
  }
  dispatchEventWithEnableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay(
    domEventName,
    eventSystemFlags,
    targetContainer,
    nativeEvent,
  );
}

function dispatchEventWithEnableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay(
  domEventName: DOMEventName, // 小写的原生事件名
  eventSystemFlags: EventSystemFlags, // 捕获阶段触发: 4 | 冒泡阶段触发: 0
  targetContainer: EventTarget, // 委托的目标元素 根元素dom
  nativeEvent: AnyNativeEvent,
) {
  let blockedOn = findInstanceBlockingEvent(
    domEventName,
    eventSystemFlags,
    targetContainer,
    nativeEvent,
  );
  if (blockedOn === null) {
    dispatchEventForPluginEventSystem(
      domEventName,
      eventSystemFlags,
      nativeEvent,
      return_targetInst,
      targetContainer,
    );
    clearIfContinuousEvent(domEventName, nativeEvent);
    return;
  }

  if (
    queueIfContinuousEvent(
      blockedOn,
      domEventName,
      eventSystemFlags,
      targetContainer,
      nativeEvent,
    )
  ) {
    nativeEvent.stopPropagation();
    return;
  }
  // We need to clear only if we didn't queue because
  // queueing is accumulative.
  clearIfContinuousEvent(domEventName, nativeEvent);

  if ( eventSystemFlags & IS_CAPTURE_PHASE ) {
    // 如果捕获阶段要出发listener
    while (blockedOn !== null) {
      const fiber = getInstanceFromNode(blockedOn);
      if (fiber !== null) {
        attemptSynchronousHydration(fiber);
      }
      const nextBlockedOn = findInstanceBlockingEvent(
        domEventName,
        eventSystemFlags,
        targetContainer,
        nativeEvent,
      );
      if (nextBlockedOn === null) {
        dispatchEventForPluginEventSystem(
          domEventName,
          eventSystemFlags,
          nativeEvent,
          return_targetInst,
          targetContainer,
        );
      }
      if (nextBlockedOn === blockedOn) {
        break;
      }
      blockedOn = nextBlockedOn;
    }
    if (blockedOn !== null) {
      nativeEvent.stopPropagation();
    }
    return;
  }

  // This is not replayable so we'll invoke it but without a target,
  // in case the event system needs to trace it.
  dispatchEventForPluginEventSystem(
    domEventName,
    eventSystemFlags,
    nativeEvent,
    null,
    targetContainer,
  );
}

export let return_targetInst = null;

// Returns a SuspenseInstance or Container if it's blocked.
// The return_targetInst field above is conceptually part of the return value.
export function findInstanceBlockingEvent(
  domEventName: DOMEventName,// 小写的原生事件名
  eventSystemFlags: EventSystemFlags, // 捕获阶段触发: 4 | 冒泡阶段触发: 0
  targetContainer: EventTarget,// 委托的目标元素 根元素dom
  nativeEvent: AnyNativeEvent,
): null | Container | SuspenseInstance {
  // TODO: Warn if _enabled is false.

  return_targetInst = null;

  const nativeEventTarget = getEventTarget(nativeEvent);
  let targetInst = getClosestInstanceFromNode(nativeEventTarget);

  if (targetInst !== null) {
    const nearestMounted = getNearestMountedFiber(targetInst);
    if (nearestMounted === null) {
      // This tree has been unmounted already. Dispatch without a target.
      targetInst = null;
    } else {
      const tag = nearestMounted.tag;
      if (tag === SuspenseComponent) {
        const instance = getSuspenseInstanceFromFiber(nearestMounted);
        if (instance !== null) {
          // Queue the event to be replayed later. Abort dispatching since we
          // don't want this event dispatched twice through the event system.
          // TODO: If this is the first discrete event in the queue. Schedule an increased
          // priority for this boundary.
          return instance;
        }
        // This shouldn't happen, something went wrong but to avoid blocking
        // the whole system, dispatch the event without a target.
        // TODO: Warn.
        targetInst = null;
      } else if (tag === HostRoot) {
        const root: FiberRoot = nearestMounted.stateNode;
        if (isRootDehydrated(root)) {
          // If this happens during a replay something went wrong and it might block
          // the whole system.
          return getContainerFromFiber(nearestMounted);
        }
        targetInst = null;
      } else if (nearestMounted !== targetInst) {
        // If we get an event (ex: img onload) before committing that
        // component's mount, ignore it for now (that is, treat it as if it was an
        // event on a non-React tree). We might also consider queueing events and
        // dispatching them after the mount.
        targetInst = null;
      }
    }
  }
  return_targetInst = targetInst;
  // We're not blocked on anything.
  return null;
}

/**
 * 获取事件优先级
 * @param {string} domEventName // 小写的原生事件名
 * @returns {lane}
 */
export function getEventPriority(domEventName: DOMEventName): lane {
  switch (domEventName) {
    // Used by SimpleEventPlugin:
    case 'cancel':
    case 'click':
    case 'close':
    case 'contextmenu':
    case 'copy':
    case 'cut':
    case 'auxclick':
    case 'dblclick':
    case 'dragend':
    case 'dragstart':
    case 'drop':
    case 'focusin':
    case 'focusout':
    case 'input':
    case 'invalid':
    case 'keydown':
    case 'keypress':
    case 'keyup':
    case 'mousedown':
    case 'mouseup':
    case 'paste':
    case 'pause':
    case 'play':
    case 'pointercancel':
    case 'pointerdown':
    case 'pointerup':
    case 'ratechange':
    case 'reset':
    case 'resize':
    case 'seeked':
    case 'submit':
    case 'touchcancel':
    case 'touchend':
    case 'touchstart':
    case 'volumechange':
    // Used by polyfills:
    // eslint-disable-next-line no-fallthrough
    case 'change':
    case 'selectionchange':
    case 'textInput':
    case 'compositionstart':
    case 'compositionend':
    case 'compositionupdate':
    // Not used by React but could be by user code:
    // eslint-disable-next-line no-fallthrough
    case 'beforeinput':
    case 'blur':
    case 'fullscreenchange':
    case 'focus':
    case 'hashchange':
    case 'popstate':
    case 'select':
    case 'selectstart':
      return DiscreteEventPriority; // 离散事件优先级 0b0000000000000000000000000000001;
    case 'drag':
    case 'dragenter':
    case 'dragexit':
    case 'dragleave':
    case 'dragover':
    case 'mousemove':
    case 'mouseout':
    case 'mouseover':
    case 'pointermove':
    case 'pointerout':
    case 'pointerover':
    case 'scroll':
    case 'toggle':
    case 'touchmove':
    case 'wheel':
    // Not used by React but could be by user code:
    // eslint-disable-next-line no-fallthrough
    case 'mouseenter':
    case 'mouseleave':
    case 'pointerenter':
    case 'pointerleave':
      return ContinuousEventPriority; // 连续事件优先级0b0000000000000000000000000000100;
    case 'message': {
      // Scheduler中的优先级转换成react中的优先级
      const schedulerPriority = getCurrentSchedulerPriorityLevel();
      switch (schedulerPriority) {
        case ImmediateSchedulerPriority:
          return DiscreteEventPriority; // 离散事件优先级 0b0000000000000000000000000000001;
        case UserBlockingSchedulerPriority:
          return ContinuousEventPriority; // 连续事件优先级 0b0000000000000000000000000000100;
        case NormalSchedulerPriority:
        case LowSchedulerPriority:
          return DefaultEventPriority; // 默认事件优先级 0b0000000000000000000000000010000;
        case IdleSchedulerPriority:
          return IdleEventPriority; // 闲置事件优先级 0b0100000000000000000000000000000;
        default: return DefaultEventPriority; // 默认优先级
      }
    }
    default: return DefaultEventPriority; // 默认优先级
  }
}
