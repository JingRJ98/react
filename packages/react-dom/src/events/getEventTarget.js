import {TEXT_NODE} from '../shared/HTMLNodeType';

/**
 * 考虑浏览器 DOM API 中的不一致的兼容
 *
 * @param {object} nativeEvent Native browser event.
 * @return {DOMEventTarget} Target node.
 */
function getEventTarget(nativeEvent) {
  // Fallback to nativeEvent.srcElement for IE9
  let target = nativeEvent.target || nativeEvent.srcElement || window;

  // Normalize SVG <use> element events #4963
  if (target.correspondingUseElement) {
    target = target.correspondingUseElement;
  }

  // 文字节点的话返回文字节点的parentNode
  return target.nodeType === TEXT_NODE ? target.parentNode : target;
}

export default getEventTarget;
