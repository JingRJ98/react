import type {AnyNativeEvent} from '../events/PluginModuleType';

// This exists to avoid circular dependency between ReactDOMEventReplaying
// and DOMPluginEventSystem.

let currentReplayingEvent = null;

export function setReplayingEvent(event: AnyNativeEvent): void {
  currentReplayingEvent = event;
}

export function resetReplayingEvent(): void {
  currentReplayingEvent = null;
}

export function isReplayingEvent(event: AnyNativeEvent): boolean {
  return event === currentReplayingEvent;
}
