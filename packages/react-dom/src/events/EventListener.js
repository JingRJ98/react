export function addEventBubbleListener(
  target: EventTarget, // 委托的目标元素 根元素dom
  eventType: string, // 小写的原生事件名
  listener: Function, // createEventListenerWrapperWithPriority创建出来的函数
): Function {
  target.addEventListener(eventType, listener, false);
  return listener;
}

export function addEventCaptureListener(
  target: EventTarget, // 委托的目标元素 根元素dom
  eventType: string, // 小写的原生事件名
  listener: Function, // createEventListenerWrapperWithPriority创建出来的函数
): Function {
  target.addEventListener(eventType, listener, true);
  return listener;
}

export function addEventCaptureListenerWithPassiveFlag(
  target: EventTarget, // 委托的目标元素 根元素dom
  eventType: string, // 小写的原生事件名
  listener: Function, // createEventListenerWrapperWithPriority创建出来的函数
  passive: boolean, // 设置为 true 时，表示 listener 永远不会调用 preventDefault()
): Function {
  target.addEventListener(eventType, listener, {
    capture: true, // 表示是否在捕获阶段触发事件, 默认false 冒泡才触发
    passive,
  });
  return listener;
}

export function addEventBubbleListenerWithPassiveFlag(
  target: EventTarget, // 委托的目标元素 根元素dom
  eventType: string, // 小写的原生事件名
  listener: Function, // createEventListenerWrapperWithPriority创建出来的函数
  passive: boolean, // 设置为 true 时，表示 listener 永远不会调用 preventDefault()
): Function {
  target.addEventListener(eventType, listener, {
    passive,
  });
  return listener;
}
