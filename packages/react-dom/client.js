/////单独暴露一个模块支持引入createRoot/////

import type {
  RootType,
  CreateRootOptions,
} from './src/client/ReactDOMRoot';

import {
  createRoot as createRootImpl,
} from './';


/**
 * 单独暴露一个模块支持引入createRoot
 * 使用:
 * import React from 'react'
 * import ReactDOM from 'react-dom/client'
 * import App from '@/App'
 * import '@/index.css'
 * ReactDOM.createRoot(document.getElementById('root')!).render(
 *   <React.StrictMode>
 *     <App />
 *   </React.StrictMode>,
)

// render(<App />, document.getElementById('root'))
 * @param {*} container
 * @param {*} options
 * @returns
 */
export function createRoot(
  container: Element | Document | DocumentFragment,
  options?: CreateRootOptions,
): RootType {
  return createRootImpl(container, options);
}
