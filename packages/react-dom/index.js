export {
  createPortal,
  createRoot,
  findDOMNode,
  flushSync,
  render,
  unmountComponentAtNode,
  unstable_batchedUpdates,
  unstable_flushControlled,
  unstable_isNewReconciler,
  unstable_renderSubtreeIntoContainer,
  unstable_runWithPriority, // DO NOT USE: Temporarily exposed to migrate off of Scheduler.runWithPriority.
  version,
} from './src/client/ReactDOM';
