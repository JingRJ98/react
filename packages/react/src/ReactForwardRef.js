import {REACT_FORWARD_REF_TYPE} from 'shared/ReactSymbols';

// forwardRef不能包裹一个memo的组件, forwardRef应该在memo里面
export function forwardRef<Props, ElementType: React$ElementType>(
  // forwardRef接受一个参数,即函数组件那个函数, 函数组件的两个参数,第一个props,第二个ref
  render: (props: Props, ref: React$Ref<ElementType>) => React$Node,
) {
  return {
    $$typeof: REACT_FORWARD_REF_TYPE, // Symbol.for('react.forward_ref');
    render,
  };;
}
