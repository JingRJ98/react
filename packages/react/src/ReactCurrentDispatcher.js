import type {Dispatcher} from 'react-reconciler/src/ReactInternalTypes';

// 不同的环境会设置不同的ReactCurrentDispatcher.current
const ReactCurrentDispatcher: {
  current: null | Dispatcher,
} = {
  current: null,
};

export default ReactCurrentDispatcher;
