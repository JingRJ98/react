import type {Fiber} from 'react-reconciler/src/ReactInternalTypes';

/**
 * ReactCurrentOwner.current指向当前正在被构造的组件
 */
const ReactCurrentOwner = {
  /**
   * @type {ReactComponent}
   */
  current: null | Fiber,
};

export default ReactCurrentOwner;
