import type {ReactNodeList} from 'shared/ReactTypes';

import {
  getIteratorFn,
  REACT_ELEMENT_TYPE,
  REACT_PORTAL_TYPE,
} from 'shared/ReactSymbols';

import {isValidElement, cloneAndReplaceKey} from './ReactElement';

const SEPARATOR = '.';
const SUBSEPARATOR = ':';

/**
 * 转义key让他能安全得作为 reactid
 * 将字符串中的=变成=0, 将:变成=2, 最后在前面加上$符号
 *
 * @param {string} key
 * @return {string}
 */
function escape(key: string): string {
  const escapeRegex = /[=:]/g;
  const escaperLookup = {
    '=': '=0',
    ':': '=2',
  };

  const escapedString = key.replace(escapeRegex, (match) => escaperLookup[match]);

  return `$${escapedString}`;
}

const userProvidedKeyEscapeRegex = /\/+/g;
function escapeUserProvidedKey(text: string): string {
  return text.replace(userProvidedKeyEscapeRegex, '$&/');
}

/**
 * 由element.key生成一个新的key
 * @param {*} element
 * @param {number} index
 * @return {string}
 */
function getElementKey(element: any, index: number): string {
  if (typeof element === 'object' && element !== null && element.key != null) {
    return escape(`${element.key}`);
  }
  return index.toString(36);
}

function mapIntoArray(
  children: ?ReactNodeList, // props中的children子组件
  array: Array<React$Node>, // 最终结果数组
  escapedPrefix: string,
  nameSoFar: string,
  callback: (?React$Node) => ?ReactNodeList,
): number {
  const type = typeof children;

  if (type === 'undefined' || type === 'boolean') {
    children = null;
  }

  let invokeCallback = false;

  if (children === null) {
    invokeCallback = true;
  } else {
    switch (type) {
      case 'string':
      case 'number':
        invokeCallback = true;
        break;
      case 'object':
        switch (children.$$typeof) {
          case REACT_ELEMENT_TYPE:
          case REACT_PORTAL_TYPE:
            invokeCallback = true;
        }
    }
  }

  if (invokeCallback) {
    const child = children;
    let mappedChild = callback(child);
    // 如果children只有一个元素, 则将名称视为包装在数组中，以便在孩子数量增加时保持一致
    const childKey = nameSoFar === '' ? SEPARATOR + getElementKey(child, 0) : nameSoFar;
    if (Array.isArray(mappedChild)) {
      let escapedChildKey = '';
      if (childKey != null) {
        escapedChildKey = escapeUserProvidedKey(childKey) + '/';
      }
      mapIntoArray(mappedChild, array, escapedChildKey, '', c => c);
    } else if (mappedChild != null) {
      if (isValidElement(mappedChild)) {
        mappedChild = cloneAndReplaceKey(
          mappedChild,
          // Keep both the (mapped) and old keys if they differ, just as
          // traverseAllChildren used to do for objects as children
          escapedPrefix +
            // $FlowFixMe Flow incorrectly thinks React.Portal doesn't have a key
            (mappedChild.key && (!child || child.key !== mappedChild.key)
              ? // $FlowFixMe Flow incorrectly thinks existing element's key can be a number
                // eslint-disable-next-line react-internal/safe-string-coercion
                escapeUserProvidedKey(`${mappedChild.key}`) + '/'
              : '') +
            childKey,
        );
      }
      array.push(mappedChild);
    }
    return 1;
  }

  let child;
  let nextName;
  let subtreeCount = 0; // Count of children found in the current subtree.
  const nextNamePrefix =
    nameSoFar === '' ? SEPARATOR : nameSoFar + SUBSEPARATOR;

  if (Array.isArray(children)) {
    for (let i = 0; i < children.length; i++) {
      child = children[i];
      nextName = nextNamePrefix + getElementKey(child, i);
      subtreeCount += mapIntoArray(
        child,
        array,
        escapedPrefix,
        nextName,
        callback,
      );
    }
  } else {
    const iteratorFn = getIteratorFn(children);
    if (typeof iteratorFn === 'function') {
      const iterableChildren: Iterable<React$Node> & {
        entries: any,
      } = (children: any);

      const iterator = iteratorFn.call(iterableChildren);
      let step;
      let ii = 0;
      while (!(step = iterator.next()).done) {
        child = step.value;
        nextName = nextNamePrefix + getElementKey(child, ii++);
        subtreeCount += mapIntoArray(
          child,
          array,
          escapedPrefix,
          nextName,
          callback,
        );
      }
    }
  }

  return subtreeCount;
}

type MapFunc = (child: ?React$Node) => ?ReactNodeList;

/**
 * 调用 Children.map(children, fn, thisArg?) 可以对 children 中的每个子节点进行映射或转换。
 *
 * See https://zh-hans.react.dev/reference/react/Children#transforming-children
 *
 * @param {?*} children props中的children子组件
 * func和数组的map方法中的回调类似。当这个函数执行时，对应的子节点和其下标将分别作为函数的第一、第二个参数，下标从 0 开始自增。
 * 你需要使这个映射函数返回一个 React 节点，它可以是一个空节点（null，undefined）
 * @param {function(*, int)} func
 * @param {*} context 可选参数, fn 函数绑定 this。默认值为 undefined。
 * @return {object} 返回一个由 fn 函数返回节点组成的一维数组。这个数组将包含除 null 和 undefined 以外的所有节点。
 *
 * @example
 * <RowList>
 *   <p>这是第一项。</p>
 *   <p>这是第二项。</p>
 *   <p>这是第三项。</p>
 * </RowList>
 *
 * RowList里面使用Children.map处理children时, 会依次对每一个p元素进行callback的处理
 * 注意1
 * children在有多个子元素的时候是一个数组, 但是不能真当数组用, 必须使用Children.map, Children.forEach等方法来特殊处理
 * 如果children只有一个子元素的时候就不是数组
 *
 * 注意2
 * Children的迭代处理只能拿到同一级的子元素, 无法拿到子元素的子元素
 *
 * 推荐方案
 * 实际开发中不建议使用Children的方法, 会削弱代码健壮性, 而是把需要的数据通过props传入, 例如一个对象数组, 再用这个数组去map生成子组件
 *
 */
function mapChildren(
  children: ?ReactNodeList,
  func: MapFunc,
  context: mixed,
): ?Array<React$Node> {
  if (children == null) return null;

  const result = [];
  let count = 0;
  mapIntoArray(children, result, '', '', function(child) {
    // return func(child, count++);
    return func.call(context, child, count++);
  });
  return result;
}

/**
 * 统计children的元素个数
 *
 * @param {?*} children Children tree container.
 * @return {number} The number of children.
 */
function countChildren(children: ?ReactNodeList): number {
  let n = 0;
  mapChildren(children, () => {
    n++;
  });
  return n;
}

type ForEachFunc = (child: ?React$Node) => void;

/**
 * Iterates through children that are typically specified as `props.children`.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrenforeach
 *
 * The provided forEachFunc(child, index) will be called for each
 * leaf child.
 *
 * @param {?*} children Children tree container.
 * @param {function(*, int)} forEachFunc
 * @param {*} forEachContext Context for forEachContext.
 */
function forEachChildren(
  children: ?ReactNodeList,
  forEachFunc: ForEachFunc,
  forEachContext: mixed,
): void {
  mapChildren(
    children,
    forEachFunc,
    forEachContext,
  );
}

/**
 * Flatten a children object (typically specified as `props.children`) and
 * return an array with appropriately re-keyed children.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrentoarray
 */
function toArray(children: ?ReactNodeList): Array<React$Node> {
  return mapChildren(children, child => child) || [];
}

/**
 * Returns the first child in a collection of children and verifies that there
 * is only one child in the collection.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrenonly
 *
 * The current implementation of this function assumes that a single child gets
 * passed without a wrapper, but the purpose of this helper function is to
 * abstract away the particular structure of children.
 *
 * @param {?object} children Child collection structure.
 * @return {ReactElement} The first and only `ReactElement` contained in the
 * structure.
 */
function onlyChild<T>(children: T): T {
  return children;
}

export {
  forEachChildren as forEach,
  mapChildren as map,
  countChildren as count,
  onlyChild as only,
  toArray,
};
