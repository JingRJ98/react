type RefObject = {
  current: any
};

// 具有单个可变值的不可变对象
// FC推荐使用useRef替代
export function createRef(): RefObject {
  return {
    current: null,
  };
}
