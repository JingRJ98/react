import ReactSharedInternals from 'react/src/ReactSharedInternals';
import {REACT_ELEMENT_TYPE} from 'shared/ReactSymbols';

const ReactCurrentOwner = ReactSharedInternals.ReactCurrentOwner;

const RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true,
};

function hasValidRef(config) {
  return config.ref !== undefined;
}

function hasValidKey(config) {
  return config.key !== undefined;
}

/**
 * 创建新 React 元素的工厂方法。 这不再遵循类模式，因此不要使用 new 来调用它。
 * 检查某个东西是否是React元素应该检查 $$typeof属性是否是Symbol.for('react.element')
 *
 * @param {string | ClassComponent | FunctionComponent} type 原生html元素字符串或者组件
 * @param {string} key html元素/组件的key
 * @param {string|object} ref
 * @param {*} self
 * @param {*} source
 * @param {*} owner ReactCurrentOwner.current,
 * @param {Object} props
 */
const ReactElement = function(type, key, ref, self, source, owner, props) {
  return {
    // 该标签使我们能够唯一地将其标识为 React 元素
    $$typeof: REACT_ELEMENT_TYPE, // Symbol.for('react.element');

    // 属于元素的内置属性
    type,
    key,
    ref,
    props,

    // 记录负责创建该元素的组件
    _owner: owner,
  };
};

/**
 * 整体和createElement函数类似
 * 区别在于jsx函数讲children放到 config中了
 * 第三个参数是指html元素或者React组件的key(这个key被react征用了, 所以理论上不应该存在)
 *
 * type如果是原生的dom元素, 是一个字符串例如'div', 'button'等
 * 如果是自定义的组件, 是一个 classComponent 或者 functionComponent 大写首字母
 * @param {string | ClassComponent | FunctionComponent} type
 * @param {object} props
 * @param {string} maybeKey
 */
export function jsx(type, config, maybeKey) {
  let propName;

  // Reserved names are extracted
  const props = {};

  let key = null;
  let ref = null;

  if (maybeKey !== undefined) {
    key = `${maybeKey}`;
  }

  // 处理config上特殊的例如key, ref等属性, 重新拿变量保存下来
    // hasValidRef 即判断 config.ref !== undefined; 下面同理
  if (hasValidKey(config)) {
    key = `${config.key}`;
  }

  if (hasValidRef(config)) {
    ref = config.ref;
  }

  // 除了上面特殊的属性之外  整下的props的内容存入props对象中
  for (propName in config) {
    if (config.hasOwnProperty(propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
      // 如果是config对象自己的属性, 并且是不是key,ref, __self,__source  这些内建属性, 就把这组key-value添加到props对象中
      props[propName] = config[propName];
    }
  }

  // type如果是原生的html元素 只是一个字符串, 如果是component, 那么是 classConent或者functionComponent, 可能有defaultProps
  if (type && type.defaultProps) {
    const defaultProps = type.defaultProps;
    for (propName in defaultProps) {
      if (props[propName] === undefined) {
        props[propName] = defaultProps[propName];
      }
    }
  }

  return ReactElement(
    type,
    key,
    ref,
    undefined,
    undefined,
    ReactCurrentOwner.current,
    props,
  );
}
