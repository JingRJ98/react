import { REACT_ELEMENT_TYPE } from 'shared/ReactSymbols';

import ReactCurrentOwner from './ReactCurrentOwner';

const RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true,
};

function hasValidRef(config) {
  return config.ref !== undefined;
}

function hasValidKey(config) {
  return config.key !== undefined;
}

/**
 * 创建新 React 元素的工厂方法。 这不遵循类模式，因此不要使用 new 来调用它
 * 检查某个东西是否是React元素应该检查 $$typeof属性是否是Symbol.for('react.element')
 *
 * @param {string | ClassComponent | FunctionComponent} type 原生html元素字符串或者组件
 * @param {string} key html元素/组件的key
 * @param {string|object} ref
 * @param {*} self A *temporary* helper to detect places where `this` is
 * different from the `owner` when React.createElement is called, so that we
 * can warn. We want to get rid of owner and replace string `ref`s with arrow
 * functions, and as long as `this` and owner are the same, there will be no
 * change in behavior.
 * @param {*} source An annotation object (added by a transpiler or otherwise)
 * indicating filename, line number, and/or other information.
 * @param {*} owner ReactCurrentOwner.current
 * @param {Object} props
 */
const ReactElement = function (type, key, ref, self, source, owner, props) {
  return {
    // 该标签使我们能够唯一地将其标识为 React 元素
    $$typeof: REACT_ELEMENT_TYPE, // Symbol.for('react.element');

    // type属性表示节点的种类
    // 它的值可以是字符串(代表div,span等 原生dom 节点),
    // 函数(代表function, class等组件节点)
    // 或者内部定义的节点类型(portal,context,fragment等), React内部定义节点类型在/shared/ReactSymbols.js中 都是symbol
    type,
    // key属性在reconciler阶段会用到
    // 所有的ReactElement对象都有 key 属性(且其默认值是 null, 这点十分重要, 在 diff 算法中会使用到).
    key,
    ref,
    props,

    // 记录创建本对象的Fiber节点, 还未与Fiber树关联之前, 该属性为null
    _owner: owner,
  };
};

/**
 * Create and return a new ReactElement of the given type.
 * See https://reactjs.org/docs/react-api.html#createelement
 */
// type如果是原生的dom元素, 是一个字符串例如'div', 'button'等
// 如果是自定义的组件, 是一个 classComponent 或者 functionComponent 大写首字母
// config是props的内容
export function createElement(type, config, children) {
  let propName;

  // 提取保留名称
  const props = {};

  let key = null;
  let ref = null;
  let self = null;
  let source = null;

  if (config != null) {
    // 处理config上特殊的例如key, ref等属性, 重新拿变量保存下来
    // hasValidRef 即判断 config.ref !== undefined; 下面同理
    if (hasValidRef(config)) {
      ref = config.ref;
    }
    if (hasValidKey(config)) {
      key = `${config.key}`;
    }

    // 这两个不太重要
    self = config.__self === undefined ? null : config.__self;
    source = config.__source === undefined ? null : config.__source;

    // 除了上面特殊的属性之外  整下的props的内容存入props对象中
    for (propName in config) {
      if (config.hasOwnProperty(propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        // 如果是config对象自己的属性, 并且是不是key,ref, __self,__source  这些内建属性, 就把这组key-value添加到props对象中
        props[propName] = config[propName];
      }
    }
  }

  // 除了type 和config 剩下不管传多少个参数都当成children
  const childrenLength = arguments.length - 2;
  if (childrenLength === 1) {
    props.children = children;
  } else if (childrenLength > 1) {
    const childArray = Array(childrenLength);
    for (let i = 0; i < childrenLength; i++) {
      childArray[i] = arguments[i + 2];
    }
    props.children = childArray;
  }

  // type如果是原生的html元素 只是一个字符串, 如果是component, 那么是 classConent或者functionComponent, 可能有defaultProps
  if (type && type.defaultProps) {
    const defaultProps = type.defaultProps;
    for (propName in defaultProps) {
      if (props[propName] === undefined) {
        // 如果一个props属性是undefined 就会使用默认props
        props[propName] = defaultProps[propName];
      }
    }
  }

  return ReactElement(
    type,
    key,
    ref,
    self,
    source,
    ReactCurrentOwner.current,
    props,
  );
}

export function cloneAndReplaceKey(oldElement, newKey) {
  const newElement = ReactElement(
    oldElement.type,
    newKey,
    oldElement.ref,
    oldElement._self,
    oldElement._source,
    oldElement._owner,
    oldElement.props,
  );

  return newElement;
}

/**
 * 类似和creatElement函数的流程
 * Clone and return a new ReactElement using element as the starting point.
 */
export function cloneElement(element, config, children) {
  let propName;

  const props = Object.assign({}, element.props);
  let key = element.key;
  let ref = element.ref;
  const self = element._self;
  const source = element._source;
  let owner = element._owner;

  if (config != null) {
    if (hasValidRef(config)) {
      // Silently steal the ref from the parent.
      ref = config.ref;
      owner = ReactCurrentOwner.current;
    }
    if (hasValidKey(config)) {
      key = `${config.key}`;
    }

    let defaultProps;
    if (element.type && element.type.defaultProps) {
      defaultProps = element.type.defaultProps;
    }
    for (propName in config) {
      if (config.hasOwnProperty(propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        if (config[propName] === undefined && defaultProps !== undefined) {
          props[propName] = defaultProps[propName];
        } else {
          props[propName] = config[propName];
        }
      }
    }
  }

  const childrenLength = arguments.length - 2;
  if (childrenLength === 1) {
    props.children = children;
  } else if (childrenLength > 1) {
    const childArray = Array(childrenLength);
    for (let i = 0; i < childrenLength; i++) {
      childArray[i] = arguments[i + 2];
    }
    props.children = childArray;
  }

  return ReactElement(element.type, key, ref, self, source, owner, props);
}

/**
 * 判断 object 是 ReactElement.
 * @param {?object} object
 * @return {boolean}
 */
export const isValidElement = (object) => (
  typeof object === 'object' &&
  object !== null &&
  object.$$typeof === REACT_ELEMENT_TYPE
);
