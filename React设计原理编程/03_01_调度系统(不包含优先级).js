const workList = []
// 调度
const schedule = () => {
  // 从列表中取出一个work
  const work = workList.pop()

  if(work){
    perform(work)
  }
}

// 执行
const perform = (work) => {
  // 一个work的数据结构为要做多少次事  和具体的事
  while(work.count--){
    // 具体的事
    insertItem(work.count)
  }


  //////// 调度的关键是执行的过程中需要接着调度 ///////////
  schedule()
}


// 一件具体的事(在document中插入一个指定内容的span)
const insertItem = (content) => {
  const ele = document.createElement('span')
  ele.innerText = content
  document.appendChild(ele)
}





