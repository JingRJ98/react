import { IdlePriority, ImmediatePriority } from "../packages/scheduler/src/SchedulerPriorities"
import { unstable_shouldYield } from "../packages/scheduler/src/forks/Scheduler"

const workList = []
let prevPriority = IdlePriority // 上一次执行的work的优先级
let curCallback // 当前调度的callback

// 调度
const schedule = () => {
  // 尝试获取当前正在调度的callback
  const cbNode = getFirstCallbackNode()

  // 从列表中取出优先级最高的work
  const work = workList.sort((a, b) => {
    // 引入优先级, 对优先级从小到大排序
    return a.priority - b.priority
  })[0]

  if (!work) {
    // 没有work需要调度 返回
    curCallback = null
    cbNode && cancelCallback(cbNode)
    return
  }

  const { priority } = work

  if (priority === prevPriority) {
    // 当前任务的优先级和上一次work的优先级相同 退出调度
    return
  }

  // 准备调度当前最高优先级的work
  // 调度之前 如果有工作正在进行 先中断
  cbNode && cancelCallback(cbNode)

  curCallback = unstable_scheduleCallback(priority, perform.bind(null, work))
}

const cancelCallback = (task) => {
  task.callback = null
}

// 执行
const perform = (work) => {
  const needSync = work.priority === ImmediatePriority || didTimeout

  // 一个work的数据结构为要做多少次事  和具体的事
  while ((needSync || !unstable_shouldYield()) && work.count--) {
    // 具体的事
    insertItem(work.count)
  }

  prevPriority = work.priority

  // 不一定是count耗尽才到这 如果说时间中断了 或者没超时
  if(!work.count){
    // 删除这个work
    const idx = workList.indexOf(work)
    workList.splice(idx, 1)
    // 重置优先级
    prevPriority = IdlePriority
  }

  const prevCallback = curCallback
  //////// 调度的关键是执行的过程中需要接着调度 ///////////
  schedule()
  const newCallback = curCallback

  if(prevCallback === newCallback){
    // callback没变 说明是同一个work 只不过是时间(5ms)用尽了
    // 返回的函数会被shcedule继续调用
    return perform.bind(null, work)
  }
}


// 一件具体的事(在document中插入一个指定内容的span)
const insertItem = (content) => {
  const ele = document.createElement('span')
  ele.innerText = content
  document.appendChild(ele)
}





