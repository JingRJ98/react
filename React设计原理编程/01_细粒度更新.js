/**
 * 细粒度更新
 * 即不需要指定依赖项 能够自动实现对依赖项的追踪变化的技术
 */

const effectStack = []
function useState(value) {
  // 保存本state被多少个effect使用
  const subs = new Set()

  const getter = () => {
    // 获取所在的effect环境 判断getter执行的时候是否处于某个effect中
    const ef = effectStack[effectStack.length - 1]
    if (ef) {
      // 建立发布订阅关系
      subScribe(ef, subs)
    }
    return value
  }
  const setter = (newValue) => {
    value = newValue
    // 发布 通知所有用了本state的effect执行回调
    for (const ef of [...subs]) {
      ef.execute()
    }
  }

  return [getter, setter]
}

// 实现useEffect
// 1. useEffect执行后回调立即执行
// 2. 依赖项变化后 回调函数执行
// 3. 不需要指定依赖项 自动跟踪
// 最关键的是回调中调用getter的时候 自动订阅该state的变化
// 当该state的setter(在useState中)执行时, 向所有订阅了该state变化的effect发布通知

function useEffect(cb) {
  const effect = {
    execute,
    deps: new Set()
  }
  const execute = () => {
    // 重置依赖
    cleanup(effect)
    // cb执行前将effect保存进栈中 是为了当前state处于哪个effect环境中(栈顶的)
    effectStack.push(effect)

    try {
      cb?.()
    } finally {
      // cb执行后将栈顶更新
      effectStack.pop()
    }
  }
  // 立即执行一次 建立订阅发布关系
  execute()
}

// 当前的effect和依赖的subs 两者之间互相解除关系
function cleanup(effect) {
  for (const subs fo effect.deps) {
    // 把自己从自己依赖的subs 每一个都移除和自己的关系
    subs.delete(effect)
  }
  // 清空自己和依赖项的关系
  effect.deps.clear()
}

function subScribe(effect, subs) {
  // 互相依赖
  subs.add(effect)
  effect.deps.add(subs)
}












