class SyntheticEvent {
  constructor(e) {
    // 保存原生事件对象
    this.nativeEvent = e
  }

  stopPropagation() {
    this._stopPropagation = true
    if (this.nativeEvent.stopPropagation) {
      this.nativeEvent.stopPropagation()
    }
  }
}

// 1.在根元素绑定事件类型对应的事件回调, 所有子孙元素触发该事件都委托给根元素的事件回调处理
/**
 * 使用
 * const root = document.querySelector('#root')
 * 在根元素处执行addEvent(root, 'click')
 */
const addEvent = (container, type) => {
  container.addEventListener(type, (e) => {
    dispatchEvent(e, type.toUpperCase(), container)
  })
}

const dispatchEvent = (e, type) => {
  // se就是一个包装的e
  // new函数的执行过程? 返回一个实例
  const se = new SyntheticEvent(e)
  const ele = e.target

  // 2. 通过dom元素找到对应的fiberNode
  let fiber
  for (let prop in ele) {
    if (prop.toLowerCase().includes('fiber')) {
      fiber = ele[prop]
    }
  }

  // 3. 收集路径中该事件的所有回调函数
  const paths = collectPaths(type, fiber)

  // 4. 捕获阶段的实现
  triggerEventFlow(paths, type + 'CAPTURE', se)

  // 5. 如果没有阻止冒泡 进行冒泡
  if (!se._stopPropagation) {
    triggerEventFlow(paths.reverse(), type + 'CAPTURE', se)
  }
}

// 从当前fiber节点一致向上遍历直到HostRootFiber
// 收集遍历过程中fiberNode.memoizedProps属性内保存的对应事件回调
const collectPaths = (type, fiber) => {
  // paths数据结构如下
  /**
   * [
   *   {
   *      CLICK: callback
   *   }
   * ]
   */
  const paths = []

  while (fiber.tag !== 3) {
    const { memoizedProps, tag } = fiber

    if (tag === 5) {
      // 原生dom元素对应的fibernode
      // 构造全大写的事件名
      const eventName = `on${type}`.toUpperCase()
      if (memoizedProps && Object.keys(memoizedProps).includes(eventName)) {
        // 如果路上发现当前fiber节点包含对应的事件回调, 收录进paths中
        paths.push({
          [type.toUpperCase()]: memoizedProps[eventName]
        })
      }
    }

    fiber = fiber.return
  }

  return paths
}

// 返回一个数组, 其中每个元素的事件名是该回调函数的名字(比如点击了最里面元素的ONCLICK)
// 由于冒泡的缘故, 从底向上遍历的过程中手机fiber节点中的ONCLICK事件, 这些事件都要被执行

const triggerEventFlow = (paths, type, se) => {
  for (let i = paths.length - 1; i >= 0; i--) {
    const pathNode = paths[i]
    const callback = pathNode[type]

    if (callback) {
      // 显然回调函数必有的一个参数是e(这里是合成事件的se)
      callback.call(null, se)

      if (se._stopPropagation) {
        // 禁止冒泡的话 只会捕获到目标元素 不会继续冒泡
        break
      }
    }
  }
}


