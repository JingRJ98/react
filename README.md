# React - V18.2.0
>`isaac --2023.10.07`
* **[中文官方文档](https://zh-hans.react.dev/)**
* **[英文官方文档](https://reactjs.org/)**

# 对源码的删减
- 删除了所有dev环境相关的代码, 往往用于本地调试
- 删除了部分SSR相关代码 (可能)
- 删除了报错提示相关, 例如报错hook
只有在异常情况下会将hook改为报错的函数 第一次阅读源码时不关注这个
```js
function throwInvalidHookError() {
  // eslint-disable-next-line react-internal/prod-error-codes
  throw new Error(
    'Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for' +
    ' one of the following reasons:\n' +
    '1. You might have mismatching versions of React and the renderer (such as React DOM)\n' +
    '2. You might be breaking the Rules of Hooks\n' +
    '3. You might have more than one copy of React in the same app\n' +
    'See https://reactjs.org/link/invalid-hook-call for tips about how to debug and fix this problem.',
  );
}

export const ContextOnlyDispatcher: Dispatcher = {
  readContext,

  useCallback: throwInvalidHookError,
  useContext: throwInvalidHookError,
  useEffect: throwInvalidHookError,
  useImperativeHandle: throwInvalidHookError,
  useInsertionEffect: throwInvalidHookError,
  useLayoutEffect: throwInvalidHookError,
  useMemo: throwInvalidHookError,
  useReducer: throwInvalidHookError,
  useRef: throwInvalidHookError,
  useState: throwInvalidHookError,
  useDeferredValue: throwInvalidHookError,
  useTransition: throwInvalidHookError,
  useId: throwInvalidHookError,

  unstable_isNewReconciler: false,
};
```


## react (接口层)
react 的核心 api 都位于 packages/react 文件夹下，包括 createElement、memo、context 以及 hooks 等，凡是通过 react 包引入的 api，都位于此文件夹下。
定义了react中的各个数据的类型, _更像数据结构_
各个hooks
createElement创建了什么数据结构
Fragment
Suspend

## scheduler
核心职责只有 1 个, 就是执行回调.
把react-reconciler提供的回调函数, 包装到一个任务对象中.
在内部维护一个任务队列, 优先级高的排在最前面.
循环消费任务队列, 直到队列清空.


## react-conciler
有 3 个核心职责
1. 装载渲染器, 渲染器必须实现HostConfig协议(如: react-dom), 保证在需要的时候, 能够正确调用渲染器的 api, 生成实际节点(如: dom节点).
2. 接收react-dom包(初次render)和react包(后续更新setState)发起的更新请求.
3. 将fiber树的构造过程包装在一个回调函数中, 并将此回调函数传入到scheduler包等待调度.

## react-dom
有 2 个核心职责:
1. 浏览器环境下的渲染
2. 实现HostConfig协议(源码在 ReactDOMHostConfig.js 中), 能够将react-reconciler包构造出来的fiber树表现出来, 生成 dom 节点(浏览器中), 生成字符串(ssr).


## shared：定义了 react 的公共方法和变量


## flow类型校验报错
需要在根目录添加.vscode/settings.json
```js
{
  "javascript.validate.enable": false
}
```

## log
- 删除了Copyright等无关注释
- 删除了__DEV__相关的报错信息代码
- 删除了代码中定义了但未使用的变量, 导入了但未使用的代码
- 修改了eslint提示的错误代码(例如未修改的变量将`let`改为`const`)
- 合并了部分.old和.new文件


## question
### 为什么定义组件的时候首字母需要大写, 用源码怎么解释?
在创建ReactElement描述类型的时候, 会根据不同的节点来传入不同的type参数, 如果是原生的dom, 会用字符串, 如'dom', 'p'
如果是组件, 会直接传入这个组件, 比如Component, 如果是一些React内部节点, 会传入内部节点
如果组件的首字母小写, React会认为这是个原生的节点, 只会传入字符串的组件名, 可以使用babel验证

### lane模型的优势
利用32位二进制数来表示, 位运算计算的更快, 更简单
可以更方便的判断单个任务与批量任务的优先级是否重叠(与运算不为0)
从一组任务中分离单个任务变得容易
现在有如下场景: 有 3 个任务, 其优先级 A > B > C, 正常来讲只需要按照优先级顺序执行就可以了.
但是现在情况变了: A 和 C 任务是CPU密集型, 而 B 是IO密集型(Suspense 会调用远程 api, 算是 IO 任务), 即 A(cpu) > B(IO) > C(cpu). 此时的需求需要将任务B从 group 中分离出来, 先处理 cpu 任务A和C.
两种方案
```js
// 从group中删除或增加task

/** 通过expirationTime实现 */
// 0) 维护一个链表, 按照单个task的优先级顺序进行插入
// 1) 删除单个task(从链表中删除一个元素)
task.prev.next = task.next;
// 2) 增加单个task(需要对比当前task的优先级, 插入到链表正确的位置上)
let current = queue;
while (task.expirationTime >= current.expirationTime) {
  current = current.next;
}
task.next = current.next;
current.next = task;
// 3) 比较task是否在group中
const isTaskIncludedInBatch =
  taskPriority <= highestPriorityInRange &&
  taskPriority >= lowestPriorityInRange;


/** 通过Lanes实现 */
// 1) 删除(移除)单个task
// 0001 1001 为了移除第4位的1
// 0001 1001 & ~(0000 1000)
// 即 0001 1001 & 1111 0111 = 0001 0001
batchOfTasks &= ~task;
// 2) 增加单个task
batchOfTasks |= task;
// 3) 比较task是否在group中
const isTaskIncludedInBatch = (task & batchOfTasks) !== 0;
```
### jsx和原来的createElement的区别
jsx函数通常情况下只有两个参数, 第一个代表节点的类型, 原生是字符串, 组件(必须大写字母开头)就是组件
第二个参数为对象, 里面包含了当前组件的属性, 比如className, children等, children指的是子节点的内容

createElement函数则有多个参数
前两个参数和jsx一样, 后面的所有参数都是children
React.createElement("div", null, React.createElement("fun", null), React.createElement("p", null, "dqwdqdwq"));

源代码
```js
const fun = () => {
	return <h1 className='class'>Hello</h1>
}

const Hello = () => {
	return <div>
      <Fun/>
      <p>dqwdqdwq</p>
    </div>
}
```

babel jsx编译
```js
import { jsx as _jsx } from "react/jsx-runtime";
import { jsxs as _jsxs } from "react/jsx-runtime";
const fun = () => {
  return /*#__PURE__*/_jsx("h1", {
    className: "class",
    children: "Hello"
  });
};
const Hello = () => {
  return /*#__PURE__*/_jsxs("div", {
    children: [/*#__PURE__*/_jsx(Fun, {}), /*#__PURE__*/_jsx("p", {
      children: "dqwdqdwq"
    })]
  });
};
```

babel React.createElement编译
```js
const fun = () => {
  return /*#__PURE__*/React.createElement("h1", {
    className: "class"
  }, "Hello");
};
const Hello = () => {
  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Fun, null), /*#__PURE__*/React.createElement("p", null, "dqwdqdwq"));
};
```

### jsx和Fiber有什么关系
### react17之前jsx文件为什么要声明import React from 'react'，之后为什么不需要了
### Fiber是什么，它为什么能提高性能
### 为什么hooks不能写在条件判断中
### setState是同步的还是异步的
### componentWillMount、componentWillMount、componentWillUpdate为什么标记UNSAFE
### react元素$$typeof属性什么
### react怎么区分Class组件和Function组件
### 函数组件和类组件的相同点和不同点
### 说说你对react的理解/请说一下react的渲染过程
### 聊聊react生命周期
### 简述diff算法
### react有哪些优化手段
### react为什么引入jsx
### 说说virtual Dom的理解
vdom对象比真实dom少了很多冗余属性, 体积小, 大量的dom操作慢，很小的更新都有可能引起页面的重新排列，js对象优于在内存中，处理更新起来更快，可以通过diff算法比较新老virtual Dom的差异，并且批量、异步、最小化的执行dom的变更，以提高性能
跨平台, js对象通过中间层可以映射到其他平台上(不仅仅是浏览器的dom元素)

### vdom和fiber的关系
### 你对合成事件的理解
react在根dom元素(即createRoot函数的参数)绑定了所有事件类型对应的事件回调
所有子孙事件触发的更新都会委托给根dom元素的事件回调处理
事件委托的流程
1. 找到触发事件的dom元素, 找到这个dom元素对应的fiber对象
2. 找到这个fiber节点到rootFiber节点的路径, 收集这条路径上所有注册该事件的回调函数(例如所有的onClick)
3. 反向执行一遍(即从rootFiber到fiber)收集到的所有事件的回调函数, 模拟事件捕获
4. 正向执行一遍 模拟事件冒泡

### 为什么我们的事件手动绑定this(不是箭头函数的情况)
### 为什么不能用 return false 来阻止事件的默认行为？
### react怎么通过dom元素，找到与之对应的 fiber对象的？
事件中的e.target指向触发事件的目标dom元素(currentTarget指向的是绑定事件的元素, 可能利用冒泡绑定给了祖先元素)
dom元素有一个属性, __reactFiber$' + randomKey, 指向了dom元素对应的fiber节点


### react工作流
第一步: 点击button 触发更新, 产生一个Update
第二步: scheduler接收到更新, 开始调度任务, 并把任务交给reconciler
第三步: reconciler接收到更新,进行diff,并且标记需要进行的操作
第四步: renderer接收到更新, 根据reconciler标记的操作来执行 并渲染

其中, 第二步和第三步可以被打断, 打断的情况
1出现更高优先级的任务
2time slice超时(通常5ms)
3出错


### 计算state的流程
1. 将baseUpdate和shared.pengding拼接成新链表
2. 遍历拼接后的新链表 根据workInProgressRootRendreLanes选定的优先级 基于符合优先级条件的update计算state
